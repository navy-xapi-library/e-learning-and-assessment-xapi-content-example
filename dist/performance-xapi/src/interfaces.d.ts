export interface Agent {
    name: string;
    objectType: string;
    account: {
        homePage: string;
        name: string;
    };
}
export interface Configuration {
    endpoint: string;
    auth: string;
    actor: {
        name: string;
        account: {
            homePage: string;
            name: string;
        };
        objectType: string;
    };
    platform?: string;
    strictCallbacks?: boolean;
}
export interface Result {
    error?: string;
    data?: string[];
}
export interface Response {
    error: any;
    status: any;
    response: any;
}
export interface NameAndDescription {
    language: string;
}
export interface Definition {
    name: NameAndDescription;
    description: NameAndDescription;
}
export interface ActivityContext {
    activity: {
        id: String;
        definition?: Definition;
    };
    contextActivity: {
        id: String;
        definition?: Definition;
    };
}
export interface Attachment {
    type: {
        usageType: string;
        display: string;
        description?: string;
        contentType: string;
    };
    value: string;
}
export interface Statement {
    verb: string;
    activityType: string;
    actor: any;
    activityAndContext: ActivityContext;
    registration: string;
    extensions?: Array<string>;
    options?: any;
    attachments?: Attachment;
}
