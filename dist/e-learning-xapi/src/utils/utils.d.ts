export declare const isObject: (o: any) => boolean;
export declare const getVerbID: (verb: any) => string;
export declare const parseLaunchParams: (parsedURL: any, baseconf?: any) => any;
export declare const ruuid: () => string;
