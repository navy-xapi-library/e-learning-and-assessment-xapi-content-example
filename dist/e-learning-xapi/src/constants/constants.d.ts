/** ***********************************************************************
*
* Veracity Technology Consultants CONFIDENTIAL
* __________________
*
*  2019 Veracity Technology Consultants
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Veracity Technology Consultants and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Veracity Technology Consultants
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Veracity Technology Consultants.
*/
export declare const ERROR: {
    INVALID_ACTIVITY_TYPE: string;
    COURSE_INITIALIZE_SUCCEED_LESSON_ERROR: string;
    NO_COURSE_REGISTRATION_ID_ERROR: string;
    NO_LESSON_REGISTRATION_ID_ERROR: string;
    ATTEMPT_TERMINATED_ERROR: string;
    NO_SUSPENDED_DATA_ERROR: string;
    COMPLETED_NOT_SET_ERROR: string;
    COMPLETED_NOT_TYPE_BOOLEAN_ERROR: string;
    SUCCESS_NOT_SET_ERROR: string;
    SUCCESS_NOT_TYPE_BOOLEAN_ERROR: string;
    TYPE_NOT_SET_ERROR: string;
    TYPE_NOT_TYPE_STRING_ERROR: string;
    SCORE_NOT_SET_ERROR: string;
    SCORE_NOT_TYPE_STRING_OR_NUMBER_ERROR: string;
    INTERACTION_TYPE_NOT_SET_ERROR: string;
    ID_NOT_SET_ERROR: string;
    CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR: string;
    INVALID_INTERACTION_TYPE_COMPONENT_LIST: string;
    RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR: string;
    CHOICES_NOT_TYPE_ARRAY_ERROR: string;
    SCALE_NOT_TYPE_ARRAY_ERROR: string;
    SOURCE_NOT_TYPE_ARRAY_ERROR: string;
    TARGET_NOT_TYPE_ARRAY_ERROR: string;
    STEPS_NOT_TYPE_ARRAY_ERROR: string;
    RESPONSE_VALUE_UNDEFINED_ERROR: string;
    INTERACTION_UNDEFINED_ERROR: string;
    INTERACTION_NOT_TYPE_OBJECT_ERROR: string;
    INTERACTION_ID_UNDEFINED_ERROR: string;
    INTERACTION_TYPE_UNDEFINED_ERROR: string;
    PARENT_UNDEFINED_ERROR: string;
    PARENT_NOT_TYPE_OBJECT_ERROR: string;
    PARENT_ID_NOT_TYPE_STRING_ERROR: string;
    PARENT_NAME_NOT_TYPE_STRING_ERROR: string;
    PARENT_DESCRIPTION_NOT_TYPE_STRING_ERROR: string;
    ACTIVITY_ID_UNDEFINED_ERROR: string;
    ACTIVITY_ID_NOT_TYPE_STRING_ERROR: string;
    ACTIVITY_NAME_UNDEFINED_ERROR: string;
    ACTIVITY_NAME_NOT_TYPE_STRING_ERROR: string;
    ACTIVITY_DESCRIPTION_NOT_TYPE_STRING_ERROR: string;
    GROUP_ID_UNDEFINED_ERROR: string;
    GROUP_ID_NOT_TYPE_STRING_ERROR: string;
    GROUP_NAME_UNDEFINED_ERROR: string;
    GROUP_NAME_NOT_TYPE_STRING_ERROR: string;
    GROUP_DESCRIPTION_NOT_TYPE_STRING_ERROR: string;
    EXTENSION_KEY_NOT_TYPE_STRING_OR_OBJECT_ERROR: string;
    EXTENSION_VALUE_NOT_TYPE_STRING_OR_OBJECT_ERROR: string;
    PARENT_ID_UNDEFINED_ERROR: string;
    LAUNCH_PARAMETERS_OR_CONFIGURATION_OBJECT_ERROR: string;
    ACTIVITY_AND_GROUP_OBJECT_REQUIRED_ERROR: string;
    ACTIVITY_OBJECT_REQUIRED_ERROR: string;
    GROUP_OBJECT_REQUIRED_ERROR: string;
    COURSE_IS_TERMINATED_ERROR: string;
    LESSON_ATTEMPT_NOT_INITIALIZED_BEFORE_RESUME_ERROR: string;
    LESSON_ATTEMPT_TERMINATED: string;
    LESSON_ATTEMPT_NOT_SUSPENDED_ERROR: string;
    EMPTY_ACTIVITY_STATE_ERROR: string;
    LESSON_ATTEMPT_NOT_FOUND: string;
    OTHER_RESPONSE_NOT_PROVIDED_ERROR: string;
    ATTACHMENT_NOT_PROVIDED_ERROR: string;
    EXTENSION_KEY_NOT_PROVIDED_ERROR: string;
    ATTACHMENT_PARAM_NOT_AN_ARRAY: string;
    OTHER_RESPONSE_NOT_PROVIDED: string;
    INVALID_MATCHING_PERFORMANCE_RESPONSE: string;
    GROUP_OBJECT_REQUIRED_TO_SUSPEND: string;
    GROUP_OBJECT_REQUIRED_TO_BOOKMARK: string;
};
export declare const EXTENSIONS: {
    SCHOOL_CENTER: string;
    LAUNCH_LOCATION: string;
    USER_AGENT: string;
    RESPONSE_EXPLANATION: string;
    RESPONSE_TYPE: string;
    EXTENDED_INTERACTION_TYPE: string;
};
export declare const VERB: {
    INITIALIZED: string;
    COMPLETED: string;
    PASSED: string;
    FAILED: string;
    TERMINATED: string;
    SUSPENDED: string;
    RESUMED: string;
    RESPONDED: string;
    SCORED: string;
    BOOKMARKED: string;
    VIEWED: string;
    ACCESSED: string;
    EXITED: string;
};
export declare const INTERACTION: {
    TRUE_FALSE: string;
    CHOICE: string;
    CHOICE_WITH_EXPLANATION: string;
    FILL_IN: string;
    LONG_FILL_IN: string;
    MATCHING: string;
    PERFORMANCE: string;
    SEQUENCING: string;
    LIKERT: string;
    NUMERIC: string;
    OTHER: string;
    OTHER_UPLOAD: string;
    OTHER_UPLOAD_ATTACHMENT: string;
};
export declare type INTERACTION_TYPE = {
    INTERACTION: any;
};
export declare const SCORE: {
    SCALED: string;
    RAW: string;
    MIN: string;
    MAX: string;
};
export declare const ACTIVITY_TYPES: {
    LESSON: string;
    COURSE: string;
    ASSESSMENT: string;
    SECTION: string;
    PROFILE: string;
    PAGE: string;
};
export declare enum ACTIVITY {
    COURSE = 0,
    LESSON = 1,
    ASSESSMENT = 2
}
