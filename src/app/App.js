import React, { useEffect } from "react";

// context
import {
  useApplicationDispatch,
  useApplicationState,
} from "../context/app-context";

import Layout from "../layout/layout";

import { parseLaunchParams }  from "../utils/utils";
import { eLearningSetUp } from '../examples/configuration';

function App({ data }) {
  const dispatch = useApplicationDispatch();
  const context = useApplicationState();

  useEffect(() => {
    dispatch({ type: "SET_COURSE_DATA", payload: data });
    dispatch({ type: "SET_TOTAL_LESSON", payload: data.lessons.length });
    dispatch({
      type: "SET_TOTAL_PAGES",
      payload: data.lessons[0].pages.length,
    });
    dispatch({ type: "SHOW_NAVIGATION", payload: false });

    let launchParams = parseLaunchParams(
      new URL(window.location.href),
      {}
    );
    if (
      launchParams.hasOwnProperty("endpoint") &&
      launchParams.hasOwnProperty("auth") &&
      launchParams.hasOwnProperty("actor")
    ){
      eLearningSetUp({})
      dispatch({type:"HIDE_LOGIN", payload:true})
    }
    
  }, []);

  return <>{context.state.data && <Layout />}</>;
}

export default App;



