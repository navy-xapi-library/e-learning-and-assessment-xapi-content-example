import React, { useState } from "react";
import {
  FormControl,
  FormLabel,
  Input,
  Button,
  Box,
  Stack,
  Text,
} from "@chakra-ui/core";

import { useApplicationDispatch } from "../context/app-context";
import { eLearningSetUp } from '../examples/configuration';

const Login = () => {
  const dispatch = useApplicationDispatch();
  const [formValues, setFormValues] = useState({
    endpoint: "https://example.lrs.io/xapi",
    username: "bill",
    password: "ted",
    name: "Test Testerson",
  });
  const handleChange = (e) => {
    e.preventDefault();
    const forms = document.querySelectorAll("form");
    const form = forms[0];
    let obj = {};
    [...form.elements].forEach((input) => {
      obj[input.id] = input.value !== "" ? input.value : formValues[input.id];
    });

    //pass along credentials to the eLearning lib
    eLearningSetUp(obj, () => dispatch({ type: "HIDE_LOGIN", payload: true }))

  };

  return (
    <Stack alignSelf="center" minW="300px" maxW="390px" mt="60px">
      <Text >E-learning Content Example Login</Text>
      <Box maxW="lg" borderWidth="1px" rounded="lg" overflow="hidden">
        <form onSubmit={handleChange}>
          <FormControl m="20px">
            <FormLabel htmlFor="endpoint">Endpoint</FormLabel>
            <Input id="endpoint" placeholder="https://example.lrs.io/xapi" />
            <FormLabel htmlFor="username">Username</FormLabel>
            <Input id="username" placeholder="LRS Username"/>
            <FormLabel htmlFor="password">Password</FormLabel>
            <Input id="password" placeholder="LRS Password"/>
            <FormLabel htmlFor="name">Name</FormLabel>
            <Input id="name" placeholder="Actor Name"/>
            <Button mt={4} variantColor="blue" type="submit" bg="#003366">
              Submit
            </Button>
          </FormControl>
        </form>
      </Box>
      <Text fontSize={"14px"}>
        The application can still be run if you don't provide  
        any login information, but the application will not be 
        connected to an LRS. If you have your LRS credentials 
        please provide them in the form above to connect to your LRS!
      </Text>
    </Stack>
  );
};
export default Login;
