import React from "react";

// custom components
import Content from "../content";
import Navigation from "../navigation";
import SideDrawer from "../side-drawer";
import StatementViewer from "../statement-viewer/";
import Welcome from "../examples/welcome";
import Login from "../login";
import TerminatedCourse from "../examples/lesson-attempt/terminated-course";

// context
import { useApplicationState } from "../context/app-context";

import { Flex, Text, Box } from "@chakra-ui/core";

import { getPageComponent } from "../utils/get-interactive-component";

const PageCounter = ({ currentPage, totalPages }) => (
  <Box pos="fixed" bottom="5%" left="4%" id="page-counter">
    <Text
      fontWeight="600"
      color="gray.500"
    >{`Page ${currentPage} of ${totalPages}`}</Text>
  </Box>
);

//( { title, context, component : Component, data })
const Layout = () => {
  const context = useApplicationState();
  // console.log("context", context);

  const { state } = context; // holds module data and current navigation index
  const {
    totalPages,
    currentLesson,
    currentPage,
    data,
    showWelcome,
    showTerminateCourse,
    hasLogin,
  } = state;
  const { lessons } = data;

  let index = showWelcome ? 0 : currentPage;

  let page = lessons[currentLesson].pages[index];

  const { type, title, content, label } = page;
  const {
    component,
    feedback,
    data: componentData,
  } = page.interactiveComponent;

  let DynamicComponent = null;

  if (component) {
    DynamicComponent = !hasLogin
      ? Login
      : showWelcome
      ? Welcome
      : showTerminateCourse
      ? TerminatedCourse
      : getPageComponent(component);
  }

  return (
    <>
      <SideDrawer />
      <Flex align="center" direction="column" height="90vh">
        <Content
          type={type}
          title={title}
          content={content}
          label={label}
          feedbackData={feedback}
          component={DynamicComponent}
          data={componentData}
        />
        {!showWelcome && (
          <PageCounter
            currentPage={
              showTerminateCourse ? currentPage + 2 : currentPage + 1
            }
            totalPages={showTerminateCourse ? totalPages + 1 : totalPages}
          />
        )}
        <Navigation />
        <StatementViewer />
      </Flex>
    </>
  );
};

export default Layout;
