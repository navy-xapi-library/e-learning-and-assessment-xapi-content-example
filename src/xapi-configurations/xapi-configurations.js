import { config } from "../examples/configuration";

export const ruuid = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

// TODO add https to iris that the dev or user generates
export const getStatement = (
  verb,
  activity,
  group,
  id,
  registration,
  grouping = true,
  parent = null,
  interaction = null,
  result = null
) => {
  // console.log("getStatement", verb, result, interaction);
  let stmt = {
    actor: {
      ...config.actor,
    },
    verb: {
      id: `http://adlnet.gov/expapi/verbs/${verb}`,
      display: {
        en: verb,
      },
    },
    object: {
      id: `https://navy.mil/netc/xapi/activities/lessons/${activity.id}`,
      definition: {
        name: {
          en: `${activity.name}`,
        },
        description: {
          en: `${activity.description}`,
        },
        type: "http://adlnet.gov/expapi/activities/lesson",
      },
    },
    context: {
      contextActivities: {
        category: [
          {
            id: "https://w3id.org/xapi/netc/v1.0",
            definition: {
              type: "http://adlnet.gov/expapi/activities/profile",
            },
          },
          {
            id: "https://w3id.org/xapi/scorm",
            definition: {
              type: "http://adlnet.gov/expapi/activities/profile",
            },
          },
        ],
        grouping: [
          {
            id: `https://navy.mil/netc/xapi/activities/courses/${group.id}`,
            definition: {
              name: {
                en: `${group.name}`,
              },
              description: {
                en: `${group.description}`,
              },
              type: "http://adlnet.gov/expapi/activities/course",
            },
          },
        ],
      },
      registration,      
      extensions: {
        "https://w3id.org/xapi/netc/extensions/school-center":
          "Department of Defense (DOD)",
        "https://w3id.org/xapi/navy/extensions/launch-location": "Ashore",
        "https://w3id.org/xapi/netc/extensions/user-agent": navigator.userAgent
      },
      platform: "Moodle 3.8.3"
    },
    timestamp: "2020-06-22T19:49:35.226Z",
  };

  if (!grouping) {
    stmt = {
      ...stmt,
      object: {
        ...stmt.context.contextActivities.grouping[0],
        definition: {
          ...stmt.context.contextActivities.grouping[0].definition,
          type: "http://adlnet.gov/expapi/activities/course",
        },
      },
      context: {
        ...stmt.context,
        contextActivities: {
          ...stmt.context.contextActivities,
          grouping: undefined,
          category: [
            {
              id: "https://w3id.org/xapi/netc/v1.0",
              definition: {
                type: "http://adlnet.gov/expapi/activities/profile",
              },
            },
          ],
        },
      },
    };
  }

  if (parent) {
    stmt = {
      ...stmt,
      context: {
        ...stmt.context,
        contextActivities: {
          ...stmt.context.contextActivities,
          grouping: [
            ...stmt.context.contextActivities.grouping,
            { ...stmt.object },
          ],
          parent: [
            {
              id: parent.id,
              definition: {
                name: {
                  en: parent.name,
                },
                description: {
                  en: parent.description,
                },
                type: "http://adlnet.gov/expapi/activities/assessment",
              },
            },
          ],
        },
      },
    };
  }
  if (interaction) {
    stmt = {
      ...stmt,
      object: {
        id: `https://navy.mil/netc/xapi/activities/cmi.interactions/${interaction.id}`,
        definition: {
          name: {
            en: interaction.name,
          },
          description: {
            en: interaction.description,
          },
          type: "http://adlnet.gov/expapi/activities/cmi.interaction",
          interactionType: interaction.interactiveComponent.component,
          correctResponsesPattern: interaction.correctResponsesPattern
            ? [interaction.correctResponsesPattern]
            : undefined,
          choices: interaction.choices ? interaction.choices : undefined,
        },
      },
      result,
    };
  }
  if (result && !interaction) {
    const { completion, score, success } = result;
    if (verb === "completed") {
      result = {
        completion: result.completion,
      };
    }
    if (verb === "terminated") {
      result = {
        completion: completion ? completion : undefined,
        score: score ? score : undefined,
        success: success != null ? success : undefined,
      };
    }
    if (verb === "scored") {
      result = {
        score: {
          scaled: result.score.scaled,
        },
      };
    }
    stmt = {
      ...stmt,
      result,
    };
  }
  return stmt;
};
