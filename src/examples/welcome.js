import React, { useEffect } from "react";

import {
  useApplicationDispatch,
  useApplicationState,
} from "../context/app-context";

import ELearningSingleton from './ELearningSingleton'

const Welcome = () => {
  const dispatch = useApplicationDispatch();
  const context = useApplicationState();
  const { bookmark, courseInitialized, credentials } = context.state;
  const xapi = ELearningSingleton.getInstance();
  useEffect(() => {
    dispatch({ type: "SHOW_NAVIGATION", payload: bookmark ? false : true });
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });

    // show both side menu and Statement viewer
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });

    if (bookmark) {
      dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
      dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [false, true] });
    }

    async function start() {
      // console.log("credentials", credentials, coursexapi)
      dispatch({ type: "SET_COURSE_INITIALIZED", payload: true });
      const { COURSE } = xapi.ACTIVITY;

      // set activity and group for the lesson attempt lesson
      xapi.setActivity(
        "https://navy.mil/netc/xapi/activities/lessons/a5ac4346-b659-11ea-b3de-0242ac130004",
        "Lesson Attempts",
        "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
      );
      xapi.setGroup(
        "https://navy.mil/netc/xapi/activities/courses/47823a7a-afee-42aa-c4ee-3333acac402",
        "E-Learning Content Example",
        "An interactive e-learning course on how a course implements xAPI Statements."
      );

      let result = await xapi
        .initialize(COURSE)
        .catch(error => console.log(error));

      // dispatch initialized to global state to manage the Statement viewer
      dispatch({ type: "STATEMENT", payload: xapi.statement });
    }
    // if the course is yet to be initialized then initialized!
    if (!courseInitialized) {
      start();
    }
  }, []);

  return (
    <div className="page-container">
      {bookmark ? (
        <>
          <h1 className="header">E-learning Content Example</h1>
          <p>
            You've have successfully suspended an attempt. Use the side drawer
            menu to the left and select the suspended lesson to resume.
          </p>
          <p className="top-bottom-margins">
            Click the <b>Statement Viewer Button</b> to see the{" "}
            <b>Suspended Statement</b> that was generated for this lesson.
          </p>
        </>
      ) : (
        <>
          <h1 className="header">Course Welcome Page</h1>
          <p>
            Welcome to the NETC xAPI E-learning Content Example. It’s expected
            that developers will “look under the hood” of these examples to see
            how the xAPI source code and libraries are utilized and can be
            reused for their own projects. The primary purpose of this example
            is to provide content developers with an understanding of how xAPI
            works in some of the most common e-learning scenarios:
          </p>
          <div className="o-list">
            <ol>
              <li>Courses</li>
              <li>Lesson Attempts</li>
              <li>Assessment Interactions & Results</li>
            </ol>
          </div>
          <p >
            Throughout these examples notice the conventions used in the
            Statements:
          </p>
          <div className="o-list">
            <ol>
              <li>Verbs represent the action performed</li>
              <li>
                Object IDs uniquely identify the thing the learner is
                interacting with - sections, lessons, and cmi interactions
              </li>
              <li>
                Context Registration ids are unique for the attempt and all
                Statements in the same attempt with have the same registration
                id
              </li>
              <li>
                The Context Activities category array contains the profile the
                Statement follows
              </li>
            </ol>
          </div>

          <p>
            First, let’s start with a Statement example showing how to{" "}
            <b>Initialize</b> an e-learning course. An e-learning course is an
            aggregation or collection of one or more lessons. An{" "}
            <b>Initialized Statement</b> is recommended to be communicated and
            stored in the NETC Learning Record Store (LRS) when an attempt on a
            course activity is started. Each course attempt begins with an{" "}
            <b>Initialized Statement</b> and ends with a{" "}
            <b>Terminated Statement</b>. A new{" "}
            Context Registration ID is
            created and used for the course attempt. This same{" "}
            Registration ID is used in both the <b>Initialized Statement</b> and the{" "}
            <b>Terminated Statement</b> for the course.
          </p>
          <p className="top-bottom-margins">
            Click the <b>Statement Viewer Button</b> to see the{" "}
            <b>Initialized Statement</b> that was generated for this first page
            of the example course.
          </p>
          <p style={{marginBottom:"80px"}}>
            When you have finished exploring the Statement, click the{" "}
            <b>NEXT Button</b> to continue to the next page,{" "}
            <i>“Initializing the Lesson”</i> or click one of the lessons in the
            main menu.
          </p>
        </>
      )}
    </div>
  );
};

export default Welcome;
