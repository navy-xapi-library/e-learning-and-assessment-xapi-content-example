import React, { useEffect } from "react";

import ELearningSingleton from '../ELearningSingleton'

import { useApplicationDispatch } from "../../context/app-context";

const Resumed = () => {
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();
  useEffect(() => {
    // Most of all of the xapi methods return a promise.
    // The resumeAttempt is one of them, so you can use await to wait for 
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.
    async function resume(){
      await xapi.resume().catch(error => console.log(error));

      // getBookmark will return the last location set.
      // In this example, it should be "l0|p2"
      let location = xapi.getBookmark();
      // check the console to see the location
      console.log(location);

      // dispatch resumed to global state to manage the statement viewer
      dispatch({ type: "STATEMENT", payload: xapi.statement });     
    
      // no interactions on this page so set page complete and show navigation
      dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
      dispatch({ type: "SHOW_NAVIGATION", payload: true });
      dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false});
      dispatch({type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true]});
    }   
    resume();
  }, []);


  return (
    <div className="page-container">
      <h1 className="header">Page 3: Resuming the Lesson</h1>
      <p >
        This lesson has been resumed after being suspended and a Resumed Statement was 
        issued to the LRS. The lesson also used the bookmark location stored during the suspend 
        example to go to this page.
      </p>
      <p className="top-bottom-margins">
        Click the <b>Statement Viewer Button</b> to see the <b>Resumed Statement</b> that was generated for this lesson. 
      </p>
       <p>
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to the 
        next page, <i>“Terminating the Lesson”</i>.
       </p>
    </div>
  );
};

export default Resumed;
