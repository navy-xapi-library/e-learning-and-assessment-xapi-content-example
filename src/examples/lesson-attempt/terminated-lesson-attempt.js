import React, { useEffect } from "react";

import ELearningSingleton from '../ELearningSingleton'

import {
  useApplicationDispatch,
  useApplicationState,
} from "../../context/app-context";

const TerminatedLessonAttempt = () => {
  // current state of the application
  const context = useApplicationState();
  const { lessonsComplete, courseCompleted, currentPage } = context.state;
  const xapi = ELearningSingleton.getInstance();
  // dispatch method to update global state
  const dispatch = useApplicationDispatch();
  
  //check to see if all lessons are completed and if so set course complete
  useEffect(() => {
    let courseComplete = lessonsComplete.filter((item) => item === true)
      .length;
    if (courseComplete === lessonsComplete.length) {
      dispatch({ type: "SET_COURSE_COMPLETE", payload: true });
    }
  }, lessonsComplete);

  useEffect(() => {
    // Most of all of the xapi methods return a promise.
    // The endLessonAttempt is one of them, so you can use await to wait for 
    // a response to return or use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.
    async function terminate(){
      const { LESSON } = xapi.ACTIVITY;
      await xapi.setComplete(true).catch(error => console.log(error));
      await xapi.terminate(LESSON).catch(error => console.log(error));

      // dispatch terminated to global state to manage the Statement viewer
      dispatch({ type: "STATEMENT", payload: xapi.statement });
      // set lesson complete since this is the last page of the lesson
      dispatch({ type: "SET_LESSON_COMPLETE", payload: null });

      // determine what lesson is currently being viewed
      // then point to either the lesson before or after it.
      // this behavior is being requested by the content developer.
      // enable next button
      dispatch({ type: "ENABLE_NEXT", payload: true });
      // dispatch event to hide the navigation
      dispatch({ type: "SHOW_NAVIGATION", payload: true });
    }


    terminate();
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Page {currentPage+1}: Terminating the Lesson</h1>
        <div>
          <p>
            Congratulations! You have completed this lesson. Similar to how it’s
            required to send an <b>Initialized Statement</b> when a new attempt on a lesson is
            started, a <b>Terminated Statement</b> is required to be
            communicated when an attempt on a lesson has ended. If there are
            lesson results, the <b>Terminated Statement</b> must include those
            results in the Result Object,
            such as whether it was passed, failed, completed, or if there is a
            score.
          </p>
          <p className="top-bottom-margins">
            Click the <b>Statement Viewer Button</b> to see the{" "}
            <b>Terminated Statement</b> that was generated for this lesson.
          </p>
          {courseCompleted ?
            <p>
              When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to 
              the next page, <i>“Terminating the Course”</i>.
            </p>
            :
            <p>
              When you have finished exploring the Statement, click the{" "}
              <b>NEXT Button</b> to continue to the next lesson,{" "}
              <i>“Assessment Interactions & Results”.</i>
            </p>
          }
        </div>
    </div>
  );
};

export default TerminatedLessonAttempt;
