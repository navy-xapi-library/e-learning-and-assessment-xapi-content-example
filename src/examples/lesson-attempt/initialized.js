import React, { useEffect } from "react";

import ELearningSingleton from '../ELearningSingleton'

import { useApplicationDispatch } from "../../context/app-context";

const Initialized = () => {
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();
  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // Most of all of the xapi methods return a promise.
    // startLessonAttempt is one of them so you can use await to wait for 
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.
    async function start() {
      const { LESSON } = xapi.ACTIVITY;
      const { COURSE } = xapi.ACTIVITY_TYPES;
      // set activity and group for the lesson attempt lesson
      xapi.setActivity(
        "https://navy.mil/netc/xapi/activities/lessons/a5ac4346-b659-11ea-b3de-0242ac130004",
        "Lesson Attempts",
        "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
      );
      xapi.setGroup(
        "https://navy.mil/netc/xapi/activities/courses/47823a7a-afee-42aa-c4ee-3333acac402",
        "E-Learning Content Example",
        "An interactive e-learning course on how a course implements xAPI Statements."
      );
      xapi.setParent(
        "https://navy.mil/netc/xapi/activities/courses/47823a7a-afee-42aa-c4ee-3333acac402",
        "E-Learning Content Example",
        "An interactive e-learning course on how a course implements xAPI Statements.",
        COURSE
      );
      await xapi.initialize(LESSON).catch(error => console.log(error));


      // dispatch initialized to global state to manage the statement viewer
      dispatch({ type: "STATEMENT", payload: xapi.statement });
    }
    // dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] })

    start();
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Page 1: Initializing the Lesson</h1>
      <p style={{ marginBottom: "20px" }}>
        Welcome to the e-learning lesson attempt example. In this example we’ll cover how an
        attempt on a lesson works and how data about the lesson is expressed in xAPI Statements.
      </p>
      <p>
        When this page was loaded, an <b>Initialized Statement</b> was issued. Each attempt begins with
        an <b>Initialized Statement</b> and ends with a <b>Terminated Statement</b>. An <b>Initialized Statement </b>
        is required to be communicated when an attempt on a lesson activity is started. When the
        Initialized Statement is sent, a new context registration  ID is created and used for
        the entire attempt. All statements in between the <b>Initialized Statement</b> and <b>Terminated
          Statement</b> of the lesson attempt use the same ID for context registration in order to
        associate the statements with the attempt. This is a different registration ID than what
        was used for the course.</p>
      <p className="top-bottom-margins">
        Click the <b>Statement Viewer</b> Button to see the <b>Initialized Statement</b> that was generated for
        this lesson.
      </p>
      <p>
        When you have finished exploring the statement, click the <b>NEXT Button</b> to continue to the
        next page, <i>“Suspending the Lesson”</i>.
      </p>
    </div>
  );
};

export default Initialized;
