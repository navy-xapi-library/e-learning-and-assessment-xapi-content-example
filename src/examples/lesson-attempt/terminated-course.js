import React, { useEffect, useState } from "react";

import ELearningSingleton from '../ELearningSingleton'

import { Button } from "@chakra-ui/core";
import {
  useApplicationDispatch,
  useApplicationState,
} from "../../context/app-context";

const TerminatedCourse = () => {
  const [disableButton, setDisableButton] = useState(false);
  // current state of the application
  const context = useApplicationState();
  const { currentPage, courseTerminated } = context.state;
  const xapi = ELearningSingleton.getInstance();
  // dispatch method to update global state
  const dispatch = useApplicationDispatch();


  useEffect(() => {
    // dispatch terminated to global state to manage the Statement viewer
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "SHOW_NAVIGATION", payload: false });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true});
  }, []);

  const onTerminateCourse = async () => {   
    // dispatch course terminate to global state to manage the Statement viewer
    
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false})
    dispatch({type: "SET_COURSE_TERMINATED", payload: true})
    setDisableButton(true);

    // Most of all of the ELearningLib methods return a promise.
    // endCourseAttempt is one of them so you can use await to wait for 
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.
    const { COURSE } = xapi.ACTIVITY;
    await xapi.terminate(COURSE).catch(error => console.log(error));
    // dispatch initialized to global state to manage the Statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });
  };

  const resetCourse = () =>{
    dispatch({type:"RESET_COURSE", payload:null})
  }

  return (
    <div className="page-container">
      <h1 className="header">Page {currentPage+2}: Terminating the Course</h1>
        <div>
          <p>
            Both lessons have been completed, you can now end your attempt on
            the course by issuing a<b> Terminated Statement</b>. A{" "}
            <b>Terminated Statement</b> is recommended to be communicated when
            an attempt on a course has ended. The <b>Terminated Statement</b>{" "}
            will also include the overall course results if they are known.
            Click the <b>Terminate Course Button</b> to trigger this action and
            send the Statement.
          </p>
          <Button
            className="top-bottom-margins"
            width="100%"
            onClick={onTerminateCourse}
            disabled={disableButton}
          >
            TERMINATE COURSE
          </Button>
          <p>
            Click the <b>Statement Viewer Button</b> to see the xAPI{" "}
            <b>Terminated Statement</b> that was generated for the course.
          </p>
          { courseTerminated &&
            <p className="top-bottom-margins">
              This ends the E-learning content example. Click <span className="span-btn" onClick={resetCourse}>here</span> to review again.
            </p>
          }
        </div>
    </div>
  );
};

export default TerminatedCourse;
