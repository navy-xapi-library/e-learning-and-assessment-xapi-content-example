import React, {  useEffect } from "react";

import ELearningSingleton from '../ELearningSingleton'

import { Button } from "@chakra-ui/core";
import { useApplicationDispatch } from "../../context/app-context";

const Suspended = () => {
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();
  const onButtonHandler = async () => {
    // Most of all of the xapi methods return a promise.
    // setBookmark and suspendAttempt are ones that do so you can use await to wait for 
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.
    await xapi.setBookmark("l0|p4").catch(error => console.log(error));
    
    await xapi.suspend().catch(error => console.log(error));

    // dispatch suspended to global state to manage the statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });
    // //dispatch this page is complete

    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    //dispatch set bookmark for global use in this example
    dispatch({ type: "SET_BOOKMARK", payload: "l0|p2" });
    dispatch({ type: "SHOW_WELCOME_PAGE", payload: true });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false});
  };

  // dispatch event to hide the navigation
  useEffect(() => {
    dispatch({ type: "SHOW_NAVIGATION", payload: false });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true})
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Page 2: Suspending the Lesson</h1>
      <p>
        Learners can suspend their e-learning lesson with the intent of coming back to it at a later 
        time. A <b>Suspended Statement</b> is sent when the user suspends an attempt on an e-learning 
        lesson. To bookmark a page, the content may store the location of the page using the xAPI
        Document Resource. See the NETC E-learning Profile for details on saving lesson attempt 
        state data. Click the <b>Suspend Lesson Button</b> below to trigger this action and send the Statement.
      </p>
      <Button type="button" className="top-bottom-margins" onClick={onButtonHandler}>
        SUSPEND LESSON
      </Button>
    </div>
  );
};

export default Suspended;
