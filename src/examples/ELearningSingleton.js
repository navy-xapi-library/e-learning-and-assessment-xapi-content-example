import { ELearningXAPI } from 'navy';

export default class ELearningSingleton {
    static instance;

    constructor(){}

    static getInstance(config) {
        if (!ELearningSingleton.instance) {
            ELearningSingleton.instance = new ELearningXAPI(config);
        }

        return ELearningSingleton.instance
    }
}