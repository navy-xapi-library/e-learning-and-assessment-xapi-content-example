import React, { useEffect, useState } from "react";

import ELearningSingleton from '../ELearningSingleton'

import {
  useApplicationDispatch,
  useApplicationState,
} from "../../context/app-context";

import { Button } from "@chakra-ui/core";

import '../styles.css'

const TerminatedAssessmentAttempt = () => {
  // current state of the application
  const context = useApplicationState();
  const { lessonsComplete, courseCompleted, currentPage } = context.state;

  // use to store submit button enabled state
  const [disableButton, setDisableButton] = useState(false);

  // dispatch method to update global state
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();
  
  //check to see if all lessons are completed and if so set course complete
  useEffect(() => {
    let courseComplete = lessonsComplete.filter((item) => item === true)
      .length;
    if (courseComplete === lessonsComplete.length) {
      dispatch({ type: "SET_COURSE_COMPLETE", payload: true });
    }
  }, lessonsComplete);

  useEffect(() => {    
    // determine what lesson currently being viewed
    // then point to either the lesson before or after it
    // this behavior is being requested by the content developer
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    // dispatch event to hide the navigation
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true});
  }, []);

  const onSubmit = async () => {
    // disable submit
    setDisableButton(true);   

    // Most of all of the xapi methods return a promise.
    // setScore is one of them so you can use await to wait for 
    // a response to return or use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.
    const { LESSON } = xapi.ACTIVITY;
    await xapi.terminate(LESSON).catch(error => console.log(error));
    // dispatch course terminate to global state to manage the Statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });
    dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false})
  };

  return (
    <div className="page-container">
      <h1 className="header">Page {currentPage+1}: Terminating the Lesson</h1>
        <div>
          <p>
            To indicate that an attempt on a lesson has ended, the content is required to send a 
            <b> Terminated Statement</b>. The <b>Terminated Statement</b> also includes the overall results 
            associated with the lesson, such as whether it was passed, failed, completed, and if there is a 
            score. Click the  <b>Terminate Lesson Button</b> to trigger this action and send the Statement.
          </p>
          <Button
            className="top-bottom-margins"
            width="100%"
            type="button"
            onClick={onSubmit}
            disabled={disableButton}
          >
          TERMINATE LESSON
          </Button>  
          <p className="top-bottom-margins">
            Click the <b>Statement Viewer Button</b> to see the example <b>Terminated Statement</b> that was 
            generated. The content developer is responsible for the content generating a Terminated 
            Statement for the lesson once the user ends their attempt.
          </p>
          {courseCompleted ?
            <p>
              When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to 
              the next page, “Terminating the Course”.
            </p>
            :
            <p>
              When you have finished exploring the Statement, click “Lesson Attempt” in the menu to begin 
              that example.
            </p>
          }
        </div>
    </div>
  );
};

export default TerminatedAssessmentAttempt;
