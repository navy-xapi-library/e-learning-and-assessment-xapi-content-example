import React, { useState, useEffect } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button, RadioGroup, Radio } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";

import ELearningSingleton from '../ELearningSingleton'

const ChoiceWithExplanation = () => {
  const dispatch = useApplicationDispatch();
  const [value, setValue] = React.useState("1")

  const [disableButton, setDisableButton] = useState(false);
  const [answer, setAnswer] = useState('choice01-yes');
  const [fillIn, setFillIn] = useState('');

  const xapi = ELearningSingleton.getInstance();

  const onFillIn = (e) => {
    setFillIn(e.target.value);
  }

  const onSubmit = async (e) => {
    // store all checked answers
    let answers = [];
    for (const property in answer) {
      if (answer[property]) {
        answers.push(property);
      }
    }
    // no checkbox checked then return
    if (answers.length === 0) {
      return;
    }

    // disable submit button
    setDisableButton(true);

    // set assessment answer
    const correct = answer === "choice02-no" ? true : false;

    const response = correct ? 'choice02-no' : 'choice01-yes';

    const choiceComponentList = [
      { id: "choice01-yes", description: "Yes" },
      { id: "choice02-no", description: "No, Please write in the correct location." }
    ].map(item => xapi.createInteractionComponentList(item.id, item.description));

    // create interaction object to pass to setInteractionResponse
    let interaction = xapi.createInteraction(
      INTERACTION.CHOICE_WITH_EXPLANATION,
      "https://navy.mil/netc/xapi/activities/cmi.interactions/66db8b2c-ce13-11eb-b8bc-0242ac130003",
      "Question 3",
      "Is the Blue Angels’ home base located at Norfolk, VA since 1954. If no, then where is the base located.",
      ["choice02-no"],
      choiceComponentList
    );
    try {
      // send choice interaction response 
      await xapi.sendInteraction(
        [response],
        interaction,
        interactionParent,
        fillIn
      )
    } catch (e) {
      console.log(e)
    }

    // dispatch choice to global state to manage the Statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });
    dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: correct });

    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
  };
  const onChange = (e) => {
    setAnswer(e.target.id);
    setValue(e.target.value)
  };

  useEffect(() => {
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
  }, [])

  return (
    <div>
      <h1 className="header">Page 3: Choice With Explanation</h1>
      <p>
        This is an example of a <span className="code-text">choice (interactionType)</span> (yes/no) with explanation question in the assessment.
        The answer to this question is “No” and “Pensacola”.
      </p>
      <div
        style={{ display: "flex", flexDirection: "column", marginTop: "10px" }}
      >
        <RadioGroup onChange={onChange} isInline value={value} style={{ margin: '1.5rem' }} spacing={4} defaultValue="1">
          <Radio size="lg" id='choice01-yes' value="1">Yes</Radio>
          <Radio size="lg" id='choice02-no' value="2">No</Radio>
        </RadioGroup>
        {
          answer === 'choice02-no' &&
          <textarea
            type='text'
            placeholder='Enter your explanation here.'
            value={fillIn}
            onChange={onFillIn}
            mt={2}
          />
          // <input type="text" value={fillIn} onChange={onFillIn} style={{width:'100%', height:'5rem', color:'black'}}/>
        }
        <Button
          className="top-bottom-margins"
          type="button"
          onClick={onSubmit}
          disabled={disableButton}
        >
          Submit
        </Button>
        <p>
          After answering the question, click the <b>Statement Viewer Button</b> to see the example
          <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
          the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
          <span className="code-text"> contextActivity parent</span> references the assessment Activity.
        </p>
        <p className="top-bottom-margins">
          When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to
          the <span className="code-text"> “Choice with Multiple Responses”</span> assessment question example.
        </p>
      </div>
    </div>
  );
};

export default ChoiceWithExplanation;
