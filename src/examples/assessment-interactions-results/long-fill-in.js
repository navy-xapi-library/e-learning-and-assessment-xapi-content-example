import React, { useState, useEffect } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button, Textarea  } from "@chakra-ui/core";

import {interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'


const LongFillIn = () => {
  const [answer, setAnswer] = useState("");
  const [disableButton, setDisableButton] = useState(false);
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();
  const onSubmit = async (e) => {
    if (answer === "") return;
    // disable submit button
    setDisableButton(true);

    // ELEARNINGXAPI INTERACTION RESPONDED IMPLEMENTATION 
    let interaction = xapi.createInteraction(
      INTERACTION.LONG_FILL_IN,
      "https://navy.mil/netc/xapi/activities/cmi.interactions/95d49860-ce13-11eb-b8bc-0242ac130003",      
      "Question 6",
      "What aircraft does the P-8 replace and what are the origins?"
    );
    await xapi.sendInteraction(
      [answer],
      interaction,
      interactionParent
    ).catch(error => console.log(error));

    // dispatch fill-in to global state to manage the Statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });
    // set assessment answer
    dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: answer === "blue" });
    // set page complete
    dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });  
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false});
  };
  const onChange = (e) => {
    setAnswer(e.target.value.toLowerCase());
  };

  useEffect(()=> {
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true});
  },[])

  return (
    <div className="page-container">
      <h1 className="header">Page 6: Long-Fill-In</h1>
      <p>
        This is an example of a <span className="code-text">long-fill-in (interactionType)</span> question in the assessment. 
      </p>
      <div className="top-bottom-margins">
        <h3 style={{marginBottom:'1rem'}}>What's the meaning of life?</h3>
        {/* <textarea type="text" onChange={onChange} /> */}
        <Textarea  type="text" onChange={onChange} placeholder='Enter your life story here' />
        <Button width="100%" style={{marginTop:"20px"}} type="button" onClick={onSubmit} disabled={disableButton}>
          Submit
        </Button>
      </div>
      <p>
        After answering the question, click the <b>Statement Viewer Button</b> to see the example 
        <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and 
        the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the 
        <span className="code-text"> contextActivity parent</span> references the assessment activity. 
      </p>
      <p className="top-bottom-margins">
          When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to 
          <span className="code-text"> “matching”</span> assessment question example..
      </p>
    </div>
  );
};

export default LongFillIn;
