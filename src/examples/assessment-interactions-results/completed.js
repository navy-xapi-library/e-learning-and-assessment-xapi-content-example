import React, { useState, useEffect } from "react";

import ELearningSingleton from '../ELearningSingleton'

import { useApplicationDispatch } from "../../context/app-context";

import { Button } from "@chakra-ui/core";

const Completed = () => {
  const [disableButton, setDisableButton] = useState(false);
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();

  const onSubmit = async () => {
    setDisableButton(true);

    // Most of all of the xapi methods return a promise.
    // setComplete is one of them so you can use await to wait for 
    // a response to return or use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.
    await xapi.setComplete(true).catch(error => console.log(error));
      
    // dispatch page complete, lesson completed to global state to manage the Statement viewer
    dispatch({ type: "ENABLE_NEXT", payload: true });   
    dispatch({ type: "STATEMENT", payload: xapi.statement });
    dispatch({ type: "SET_RESULT", payload: { completion: true } });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false});
  };

  useEffect(()=> {
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true});
  },[])

  return (
    <div className="page-container">
      <h1 className="header">Page 8: Completing the Lesson</h1>
      <p>
        In addition to setting the success status (pass or fail), the completion of a lesson assessment 
        can also be determined and sent using a Completed Statement. Click the <b>Complete Lesson 
        Button</b> to trigger this action and send the Statement. Note: The overall completion status of a 
        lesson assessment is communicated by using a <b>Terminated Statement</b> with 
        <span className="code-text"> result.completion</span> to <span className="code-text">true</span>.
      </p>
      <Button className="top-bottom-margins" type="button" onClick={onSubmit} disabled={disableButton}>
      COMPLETE LESSON
      </Button>
      <p>
        Click the <b>Statement Viewer Button</b> to see the example <b>Completed Statement</b> that was generated.
      </p>
      <p style={{marginTop:"20px"}}>
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to the 
        next page, “Terminating the Lesson Assessment”.
      </p>
    </div>
  );
};

export default Completed;
