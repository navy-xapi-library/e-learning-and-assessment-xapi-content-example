import React, { useState, useEffect } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button, Radio, RadioGroup } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'


const Likert = () => {
    const dispatch = useApplicationDispatch();

    const [disableButton, setDisableButton] = useState(false);
    const [value, setValue] = React.useState("1");
    const [answer, setAnswer] = React.useState('option_1');
    const xapi = ELearningSingleton.getInstance();

    const onSubmit = async (e) => {
        // disable submit button
        setDisableButton(true);

        // create likert response pattern to pass into sendInteraction
        const likertComponentList = [
            { id: "option_1", description: "OK" },
            { id: "option_2", description: "Pretty Cool" },
            { id: "option_3", description: "Gonna Change the World" }
        ].map(item => xapi.createInteractionComponentList(item.id, item.description));

        // create interaction object to pass to setInteractionResponse
        let interaction = xapi.createInteraction(
            INTERACTION.LIKERT,
            "https://navy.mil/netc/xapi/activities/cmi.interactions/8fe88cea-ce13-11eb-b8bc-0242ac130003",
            "Question 10",
            "An example of a likert question.",
            xapi.createResponsePattern(INTERACTION.LIKERT, ["option_1", "option_2", "option_3"]),
            likertComponentList
        );
        // send choice interaction response 
        await xapi.sendInteraction(
            [answer],
            interaction,
            interactionParent
        ).catch(error => console.log(error));

        // dispatch choice to global state to manage the Statement viewer
        dispatch({ type: "STATEMENT", payload: xapi.statement });
        dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: true });

        // enable next button
        dispatch({ type: "ENABLE_NEXT", payload: true });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    };
    const onChange = (e) => {
        setValue(e.target.value)
        setAnswer(e.target.id);
    };

    useEffect(() => {
        dispatch({ type: "ENABLE_NEXT", payload: false });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    }, [])

    return (
        <div>
            <h1 className="header">Page 10: Likert</h1>
            <p>
                This is an example of a <span className="code-text">likert (interactionType)</span> question in the assessment.
                The answer to this question is any of the options.
            </p>
            <div
                style={{ display: "flex", flexDirection: "column", marginTop: "10px" }}
            >
                <RadioGroup onChange={onChange} isInline value={value} style={{ margin: '1.5rem' }} spacing={4} defaultValue="1">
                    <Radio size="lg" id='option_1' value="1">OK</Radio>
                    <Radio size="lg" id='option_2' value="2">Pretty Cool</Radio>
                    <Radio size="lg" id='option_3' value="3">Gonna Change the World</Radio>
                </RadioGroup>
                <Button
                    className="top-bottom-margins"
                    type="button"
                    onClick={onSubmit}
                    disabled={disableButton}
                >
                    Submit
                </Button>
                <p>
                    After answering the question, click the <b>Statement Viewer Button</b> to see the example
                    <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
                    the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
                    <span className="code-text"> contextActivity parent</span> references the assessment Activity.
                </p>
                <p className="top-bottom-margins">
                    When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to
                    the <span className="code-text"> numeric</span> assessment question example.
                </p>
            </div>
        </div>
    );
};

export default Likert;
