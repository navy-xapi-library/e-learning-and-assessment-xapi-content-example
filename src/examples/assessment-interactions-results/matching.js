import React, { useState, useEffect, useRef } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'

import { DragAndDrop, Droppable, Draggable } from "../../drag-n-drop";

const answersMap = {
    target1: 'source2',
    target2: 'source1'
}

const Matching = () => {
    const [answer, setAnswer] = useState("");
    const [disableButton, setDisableButton] = useState(false);
    const [matching, setMatching] = useState([]);
    const dispatch = useApplicationDispatch();
    const xapi = ELearningSingleton.getInstance();
    
    const checkComplete = () => {
        const correctAnswers = matching.filter(answer => answer.source === answersMap[answer.target])

        if (matching.length === Object.keys(answersMap).length) {
            onSubmit(matching, correctAnswers.length === Object.keys(answersMap).length)
        }
    }

    const onDrop = React.useCallback((e) => {
        e.source.style.backgroundColor = "#0062cc";
        e.source.style.color = "white";
        e.source.style.border = "none"
        e.source.style.pointerEvents = "none"
        setMatching([...matching, `${e.source.id}:${e.target.id}`])
    })

    const onSubmit = async (answers, success) => {
        // if (answer === "") return;
        // disable submit button
        setDisableButton(true);

        const matchingSourceComponentList = [
            { id: "source1", description: "5 + 5 =" },
            { id: "source2", description: "10 + 10 =" },
        ].map(item => xapi.createInteractionComponentList(item.id, item.description));

        const matchingTargetComponentList = [
            { id: "target1", description: "10" },
            { id: "target2", description: "20" },
        ].map(item => xapi.createInteractionComponentList(item.id, item.description));

        // ELEARNINGXAPI INTERACTION RESPONDED IMPLEMENTATION 
        // Since Matching has both source and target arrays as a response pattern 
        // createInteraction allows you to pass in an object with a property source and target and
        // the values being the correct response pattern create above
        let interaction = xapi.createInteraction(
            INTERACTION.MATCHING,
            "https://navy.mil/netc/xapi/activities/cmi.interactions/9d1e83e2-ce13-11eb-b8bc-0242ac130003",
            "Question 7",
            "What aircraft does the P-8 replace and what are the origins?",
            xapi.createResponsePattern(INTERACTION.MATCHING, ["source2:target1","source1:target2"]),
            { source: matchingSourceComponentList, target: matchingTargetComponentList }
        );
        await xapi.sendInteraction(
            xapi.createResponsePattern(INTERACTION.MATCHING, answers),
            interaction,
            interactionParent
        ).catch(error => console.log(error));

        // dispatch fill-in to global state to manage the Statement viewer
        dispatch({ type: "STATEMENT", payload: xapi.statement });
        // set assessment answer
        dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: answer });
        // set page complete
        dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
        dispatch({ type: "SHOW_NAVIGATION", payload: true });
        dispatch({ type: "ENABLE_NEXT", payload: true });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    };
    const onChange = (e) => {
        setAnswer(e.target.value.toLowerCase());
    };

    useEffect(() => {
        dispatch({ type: "ENABLE_NEXT", payload: false });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    }, [])

    useEffect(() => {
        checkComplete()
    }, [matching])

    return (
        <div className="page-container">
            <h1 className="header">Page 7: Matching</h1>
            <p>
                This is an example of a <span className="code-text">matching (interactionType)</span> question in the assessment.
                The answer to this question is “source1” to “target1” and “source2” to “target1”.
            </p>
            <div className="app-container">
                <DragAndDrop onDrop={onDrop} >
                    <Draggable className='drag-container'>
                        <div id='source1' className='drag-element'>source1</div>
                        <div id='source2' className='drag-element'>source2</div>
                    </Draggable>
                    <Droppable className='drop-container'>
                        <div id='target1' className='drop-element'>target1</div>
                        <div id='target2' className='drop-element'>target2</div>
                    </Droppable>
                </DragAndDrop>
            </div>
            <p>
                After answering the question, click the <b>Statement Viewer Button</b> to see the example
                <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
                the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
                <span className="code-text"> contextActivity parent</span> references the assessment activity.
            </p>
            <p className="top-bottom-margins">
                When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to
                <span className="code-text"> “performance”</span> assessment question example..
            </p>
        </div>
    );
};

export default Matching;
