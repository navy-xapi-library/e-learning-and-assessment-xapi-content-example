import React, {useEffect, useState} from "react";

import ELearningSingleton from '../ELearningSingleton'

import { Button } from "@chakra-ui/core";

import { useApplicationState, useApplicationDispatch } from "../../context/app-context";

const PassedFailed = () => {
  const context = useApplicationState();
  const dispatch = useApplicationDispatch();
  const [disableButton, setDisableButton] = useState(false);
  const xapi = ELearningSingleton.getInstance();
  const {
    state: { assessmentAnswers },
  } = context;

  let incorrect = assessmentAnswers.filter( item => item === false).length;
  let passed = incorrect > 3 ? false : true;

  const onSubmit = async () => {
    setDisableButton(true);

    // Most of all of the xapi methods return a promise.
    // setSuccess is one of them so you can use await to wait for 
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.

    await xapi.setSuccess(passed).catch(error => console.log(error));

    dispatch({ type: "STATEMENT", payload: xapi.statement });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false});
  };

  useEffect(() => {
    // set global vars
    dispatch({ type: "ENABLE_NEXT", payload: false });        
    dispatch({ type: "SET_RESULT", payload: { success: passed } });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true});
  },[]);

  return (
    <div className="page-container">
      <h1 className="header">Page 7: Passing or Failing the Lesson</h1>
      <p>
        A <b>Passed</b> or <b>Failed Statement</b> can be sent based on the criteria established in the design of 
        the content. Click the <b>Pass/Fail Lesson Button</b> to trigger this action and send the Statement.
      </p>

      <Button
        className="top-bottom-margins"
        type="button"
        onClick={onSubmit}
        disabled={disableButton}
      >
        PASS/FAIL LESSON
      </Button>
      <p>
        Click the <b>Statement Viewer Button</b> to see the example <b>Passed</b> or <b>Failed Statement</b> that 
        was generated. In this example, a <b>Passed Statement</b> is generated for the lesson if the user 
        only misses one or fewer questions in the lesson assessment. A <b>Failed Statement</b> is 
        generated for the lesson if the user misses more than one of the questions in the assessment. 
        Note: The <b>Terminated Statement</b> <span className="code-text">result</span> is used to report the authoritative success for the lesson.  
      </p>
      <p style={{marginTop:"20px"}}>
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to the 
        next page, “Completing the Lesson Assessment”.
      </p>
    </div>
  );
};

export default PassedFailed;
