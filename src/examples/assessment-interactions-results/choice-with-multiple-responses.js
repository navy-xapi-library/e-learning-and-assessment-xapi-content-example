import React, { useState, useEffect } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'


const ChoiceMultiResponse = () => {
    const dispatch = useApplicationDispatch();
    const xapi = ELearningSingleton.getInstance();

    const [disableButton, setDisableButton] = useState(false);
    const [answer, setAnswer] = useState({
        choice_1: false,
        choice_2: false,
        choice_3: false,
    });

    const onSubmit = async (e) => {
        const response = [];

        for (let ans in answer) {
            if (answer[ans]) {
                response.push(ans);
            }
        }
        // disable submit button
        setDisableButton(true);

        // set assessment answer
        const correct = response.filter(item => item === 'choice_2' || item === 'choice_3').length === 2 ? true : false;

        // create choice response pattern to pass into sendInteraction
        const choiceComponentList = [
            { id: "choice_1", description: "Answer 1" },
            { id: "choice_2", description: "Answer 2" },
            { id: "choice_3", description: "Answer 3" }
        ].map(item => xapi.createInteractionComponentList(item.id, item.description));

        // create interaction object to pass to setInteractionResponse
        let interaction = xapi.createInteraction(
            INTERACTION.CHOICE,
            "https://navy.mil/netc/xapi/activities/cmi.interactions/79cb7670-ce13-11eb-b8bc-0242ac130003",
            "Question 4",
            "The answer to this question is “Answer 2”.",
            xapi.createResponsePattern(INTERACTION.CHOICE, ["choice_2", "choice_3"]),
            choiceComponentList
        );
        // send choice interaction response 
        await xapi.sendInteraction(
            xapi.createResponsePattern(INTERACTION.CHOICE, response),
            interaction,
            interactionParent
        ).catch(error => console.log(error));

        // dispatch choice to global state to manage the Statement viewer
        dispatch({ type: "STATEMENT", payload: xapi.statement });
        dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: correct });

        // enable next button
        dispatch({ type: "ENABLE_NEXT", payload: true });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    };
    const onChange = (e) => {
        setAnswer({
            ...answer,
            [e.target.value]: e.target.checked,
        });
    };

    useEffect(() => {
        dispatch({ type: "ENABLE_NEXT", payload: false });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    }, [])

    return (
        <div>
            <h1 className="header">Page 4: Choice With Multiple Responses</h1>
            <p>
                This is an example of a <span className="code-text">choice (interactionType)</span> question in the assessment.
                The answer to this question is “answer2” and “answer3”.
            </p>
            <div
                style={{ display: "flex", flexDirection: "column", marginTop: "10px" }}
            >
                <div style={{ display: "flex", alignItems: "center" }}>
                    <input id='choice_1' type="checkbox" value="choice_1" onChange={onChange} />
                    <span style={{ marginLeft: "10px", fontSize: '1.5rem' }}>Answer 1</span>
                </div>
                <div style={{ display: "flex", alignItems: "center" }}>
                    <input id='choice_2' type="checkbox" value="choice_2" onChange={onChange} />
                    <span style={{ marginLeft: "10px", fontSize: '1.5rem' }}>Answer 2</span>
                </div>
                <div style={{ display: "flex", alignItems: "center" }}>
                    <input id='choice_3' type="checkbox" value="choice_3" onChange={onChange} />
                    <span style={{ marginLeft: "10px", fontSize: '1.5rem' }}>Answer 3</span>
                </div>
                <Button
                    className="top-bottom-margins"
                    type="button"
                    onClick={onSubmit}
                    disabled={disableButton}
                >
                    Submit
                </Button>
                <p>
                    After answering the question, click the <b>Statement Viewer Button</b> to see the example
                    <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
                    the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
                    <span className="code-text"> contextActivity parent</span> references the assessment Activity.
                </p>
                <p className="top-bottom-margins">
                    When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to
                    the <span className="code-text"> “fill-in”</span> assessment question example.
                </p>
            </div>
        </div>
    );
};

export default ChoiceMultiResponse;
