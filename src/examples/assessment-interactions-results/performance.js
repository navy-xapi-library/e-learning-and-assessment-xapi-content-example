import React, { useState, useEffect, useRef } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'


const answersMap = {
    step1: 'up',
    step2: 'down',
    step3: 'left',
    step4: 'right'
}

const Performance = () => {
    const [disableButton, setDisableButton] = useState(false);
    const [steps, setSteps] = useState([]);
    const dispatch = useApplicationDispatch();
    const xapi = ELearningSingleton.getInstance();

    const checkComplete = () => {
        const correctAnswers = steps.filter(answer => answer.response === answersMap[answer.id])

        if (steps.length === Object.keys(answersMap).length) {
            onSubmit(steps, correctAnswers.length === Object.keys(answersMap).length)
        } 
    }
    const onController = (e) => {
        setSteps([...steps, `${e.target.id}:${e.target.dataset.value}`])
    }
    const onSubmit = async (answers, success) => {

        setDisableButton(true);

        const performanceComponentList = [
            { id: "step1", description: "up" },
            { id: "step2", description: "down" },
            { id: "step3", description: "left" },
            { id: "step4", description: "right" },
        ].map(item => xapi.createInteractionComponentList(item.id, item.description));

        // ELEARNINGXAPI INTERACTION RESPONDED IMPLEMENTATION 
        let interaction = xapi.createInteraction(
            INTERACTION.PERFORMANCE,
            "https://navy.mil/netc/xapi/activities/cmi.interactions/c4642a7e-ce13-11eb-b8bc-0242ac130003",
            "Question 8",
            "Performance the steps to enter cheat code.",
            xapi.createResponsePattern(INTERACTION.PERFORMANCE, ["step1:up","step2:down","step3:left","step4:right"]),
            performanceComponentList
        );
        await xapi.sendInteraction(
            xapi.createResponsePattern(INTERACTION.PERFORMANCE, answers),
            interaction,
            interactionParent
        ).catch(error => console.log(error));

        // dispatch fill-in to global state to manage the Statement viewer
        dispatch({ type: "STATEMENT", payload: xapi.statement });
        // set assessment answer
        dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: success });
        // set page complete
        dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
        dispatch({ type: "SHOW_NAVIGATION", payload: true });
        dispatch({ type: "ENABLE_NEXT", payload: true });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    };

    useEffect(() => {
        dispatch({ type: "ENABLE_NEXT", payload: false });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    }, [])

    useEffect(() => {
        checkComplete()
    }, [steps])

    return (
        <div className="page-container">
            <h1 className="header">Page 8: Performance</h1>
            <p>
                This is an example of a <span className="code-text">performance (interactionType)</span> question in the assessment.
                The answer to this question is “up”, “down”, “left”, and “right”.
            </p>
            <div className="performance-container" style={{ height: '8rem' }}>
                <div className='controllerLeft'>
                    <div className='circle'></div>
                    <div className='crossCenter'>
                        <div id='step1' className='crossTop' onClick={onController} data-value='up'></div>
                        <div id='step2' className='crossBottom' onClick={onController} data-value='down'></div>
                        <div id='step3' className='crossLeft' onClick={onController} data-value='left'></div>
                        <div id='step4' className='crossRight' onClick={onController} data-value='right'></div>
                        <div className='crossCircle' onClick={onController}></div>
                    </div>
                </div>
            </div>
            <p>
                After answering the question, click the <b>Statement Viewer Button</b> to see the example
                <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
                the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
                <span className="code-text"> contextActivity parent</span> references the assessment activity.
            </p>
            <p className="top-bottom-margins">
                When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to
                <span className="code-text"> numeric</span> assessment question example..
            </p>
        </div>
    );
};

export default Performance;
