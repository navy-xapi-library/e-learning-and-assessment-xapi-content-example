import React, { useEffect, useState } from "react";

import {interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'

import { Button } from "@chakra-ui/core";

import {
  useApplicationDispatch,
} from "../../context/app-context";

const Numeric = () => {
  const dispatch = useApplicationDispatch();

  const [disableButton, setDisableButton] = useState(false);

  const xapi = ELearningSingleton.getInstance();
  
  const onSubmit = async (e) => {
    // grab and set user's answer
    let sliderValue = document.getElementById("bubble").innerHTML;
    // disable submit button
    setDisableButton(true);

    let interaction = xapi.createInteraction(
      INTERACTION.NUMERIC,
      "https://navy.mil/netc/xapi/activities/cmi.interactions/a40ccfba-ce13-11eb-b8bc-0242ac130003",     
      "Question 11",
      "A Numeric example.",
      ["75"]
    );
    await xapi.sendInteraction(
      [sliderValue],
      interaction,
      interactionParent
    ).catch(error => console.log(error));

    // set if user got the answer right
    dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: sliderValue === "75" });
    // dispatch choice to global state to manage the statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false});
  };

  const setBubble = (range, bubble) => {
    const val = range.value;
    const min = range.min ? range.min : 0;
    const max = range.max ? range.max : 100;
    const newVal = Number(((val - min) * 100) / (max - min));
    bubble.innerHTML = val;

    // Sorta magic numbers based on size of the native UI thumb
    bubble.style.left = `calc(${newVal}% + (${8 - newVal * 0.15}px))`;
    // console.log("setBubble", newVal)
  }

  useEffect(() => {
    // set page complete, show navigation, and statement viewer value
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true});

    const allRanges = document.querySelectorAll(".range-wrap");
    allRanges.forEach((wrap) => {
        const range = wrap.querySelector(".range");
        const bubble = wrap.querySelector(".bubble");
    
        range.addEventListener("input", () => {
          setBubble(range, bubble);
        });
        setBubble(range, bubble);
      });
    
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Page 11: Numeric</h1>
      <p>
        This is an example of a{" "}
        <span className="code-text">numeric (interactionType)</span> question in
        the assessment. The answer to this question is “75”.
      </p>
      <div className="range-wrap top-bottom-margins" style={{ width: "100%" }}>
        <input type="range" className="range" min="0" max="100" style={{ width: "100%" }}/>
        <output id="bubble" className="bubble top-bottom-margins"></output>
      </div>
      <Button
        className="top-bottom-margins-lg"
        type="button"
        onClick={onSubmit}
        disabled={disableButton}
      >
        SUBMIT
      </Button>
      <p>
        After answering the question, click the <b>Statement Viewer Button</b>{" "}
        to see the example
        <b> Responded Statement</b> that was generated. Notice the question,{" "}
        <span className="code-text">interactionType</span>, and the response.
        The response is provided in the{" "}
        <span className="code-text">result</span> Object. Also notice the
        <span className="code-text"> contextActivity parent</span> references
        the assessment Activity.
      </p>
      <p style={{ marginTop: "20px" }}>
        When you have finished exploring the Statement, click the{" "}
        <b>NEXT Button</b> to continue to the{" "}
        <span className="code-text"> other</span> assessment question example.
      </p>
    </div>
  );
};

export default Numeric;
