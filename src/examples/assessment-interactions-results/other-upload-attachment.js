import React, { useState, useEffect } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button, Input } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'


const OtherUploadAttachment = () => {
    const [state, setState] = useState({});
    const [disableButton, setDisableButton] = useState(false);
    const dispatch = useApplicationDispatch();
    const xapi = ELearningSingleton.getInstance();
    const onFormSubmit = async (e) => {
        e.preventDefault();
        const fileName = e.target.files[0].name;
        const reader = new FileReader();
        reader.onload = async (e) => {
            const arrayBuffer = e.target.result;
            console.log(e.target)
            // send file attachment to the lrs
            setState({ fileName, data: arrayBuffer });
        };
        reader.readAsArrayBuffer(e.target.files[0]);
    }


    const onSubmit = async (e) => {
        // disable submit button
        setDisableButton(true);

        const { fileName, data } = state;
        const attachment = xapi.attachment(
            data,
            "image",
            "image/jpeg",
            "Image Attachment.",
            "Uploading An Image Attachment."
        );

        // ELEARNINGXAPI INTERACTION RESPONDED IMPLEMENTATION 
        let interaction = xapi.createInteraction(
            INTERACTION.OTHER_UPLOAD_ATTACHMENT,
            "https://navy.mil/netc/xapi/activities/cmi.interactions/abad47fe-ce13-11eb-b8bc-0242ac130003",
            "Question 14",
            "An example of other upload attachment interaction type with upload."
        );
        await xapi.sendInteraction(
            [fileName],
            interaction,
            interactionParent,
            null,
            [attachment]
        ).catch(error => console.log(error));

        // dispatch fill-in to global state to manage the Statement viewer
        dispatch({ type: "STATEMENT", payload: xapi.statement });
        // set assessment answer
        // dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: answer === "51.1789, 1.8262" });
        // set page complete
        dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
        dispatch({ type: "SHOW_NAVIGATION", payload: true });
        dispatch({ type: "ENABLE_NEXT", payload: true });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    };

    useEffect(() => {
        dispatch({ type: "ENABLE_NEXT", payload: false });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    }, [])

    return (
        <div className="page-container">
            <h1 className="header">Page 14: Other - Upload With Attachment</h1>
            <p>
                This is an example of a <span className="code-text">other-upload (interactionType)</span> question in the assessment.
                Choose an image file to upload.
            </p>
            <div className="top-bottom-margins">
                <h3 style={{ marginBottom: '1rem' }}>File Name:</h3>
                <Input type="file" id="myFile" name="filename" onChange={onFormSubmit} />
                <Button width="100%" style={{ marginTop: "20px" }} type="button" onClick={onSubmit} disabled={disableButton}>
                    Submit
                </Button>
            </div>
            <p>
                After answering the question, click the <b>Statement Viewer Button</b> to see the example
                <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
                the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
                <span className="code-text"> contextActivity parent</span> references the assessment activity.
            </p>
            <p className="top-bottom-margins">
            When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to 
            the next page, “Scoring the Assessment”.
            </p>
        </div>
    );
};

export default OtherUploadAttachment;
