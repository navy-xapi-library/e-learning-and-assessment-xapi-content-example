import React, { useEffect, useState } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'


const TrueFalse = () => {
  const [answer, setAnswer] = useState("checkbox1");
  const [disableButton, setDisableButton] = useState(false);
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();

  const onSubmit = async (e) => {
    // disable submit button
    setDisableButton(true);

    let interaction = xapi.createInteraction(
      INTERACTION.TRUE_FALSE,
      "https://navy.mil/netc/xapi/activities/cmi.interactions/6fd493ee-b740-11ea-b3de-0242ac130004",      
      "Question 1",
      "Is this a True or False example?",
      ["true"]
    );
    await xapi.sendInteraction(
      [answer === "checkbox1"],
      interaction,
      interactionParent
    ).catch(error => console.log(error));

    // dispatch true-false to global state to manage the statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });

    dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: answer === "checkbox1"});
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false});
  };

  const onChange = (e) => {
    setAnswer(e.target.value);
  };

  // In this case, we are using useEffect to handle onComponentDidMount.
  // Once this component mounts it will call the startLessonAttempt once and
  // only once in the application life time.
  useEffect(() => {

    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true});
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Page 1: True-False</h1>
      <p>
        This is an example of a <span className="code-text">true-false (interactionType)</span> question in the assessment. 
        The answer to this question is “true”. 
      </p>

      <div style={{ marginTop: "20px" }}>
        <p>Is this a True or False example?</p>
        <form  style={{ marginTop: "10px" }}>
          <div className='radio-container'>
            <input
              type="radio"
              value="checkbox1"
              checked={answer === "checkbox1"}
              onChange={onChange}
            />
            <label style={{ marginLeft: "10px" }}>True</label>
          </div>

          <div className='radio-container' >
            <input
              type="radio"
              value="checkbox2"
              checked={answer === "checkbox2"}
              onChange={onChange}
            />
            <label style={{ marginLeft: "10px" }}>False</label>
          </div>
        </form>
        <Button
          width="100%"
          className="top-bottom-margins"
          type="button"
          onClick={onSubmit}
          disabled={disableButton}
        >
          Submit
        </Button>
      </div>
      <p>
        After answering the question, click the <b>Statement Viewer Button</b> to see the example 
        <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and 
        the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the 
        <span className="code-text"> contextActivity parent</span> references the assessment Activity. 
      </p>
      <p className="top-bottom-margins">
          When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to 
          the next page, “Choice”.
      </p>
    </div>
  );
};

export default TrueFalse;
