import React, { useEffect } from "react";

import ELearningSingleton from '../ELearningSingleton'


import { useApplicationDispatch } from "../../context/app-context";

const Intro = () => {
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();
  // useEffect is a lifecyle method that handles onComponentDidMount,
  // onComponentDidUpdate, and other lifecycles.
  // In this case, we are using useEffect to handle onComponentDidMount.
  // Once this component mounts it will call the startLessonAttempt once and
  // only once in the application life time.
  useEffect(() => {
    // Most of all of the xapi methods return a promise.
    // The resumeAttempt is one of them, so you can use await to wait for
    // a response to return or use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.
    async function start() {
      xapi.setActivity(
        "https://navy.mil/netc/xapi/activities/lessons/b1db16ec-b659-11ea-b3de-0242ac130004",
        "Assessment Interactions & Results",
        "Learn how assessment interactions are reported to the LRS as xAPI Statements."
      );
      xapi.setGroup(
        "https://navy.mil/netc/xapi/activities/courses/47823a7a-afee-42aa-c4ee-3333acac402",
        "E-Learning Content Example",
        "An interactive e-learning course on how a course implements xAPI Statements."
      );
      const { LESSON } = xapi.ACTIVITY;

      await xapi
        .initialize(LESSON)
        .catch((error) => console.log(error));

      // dispatch initialized to global state to manage the statement viewer
      dispatch({ type: "STATEMENT", payload: xapi.statement });
    }

    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    // dispatch event to hide the navigation
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    // disable side drawer menu items
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    //disable statement viewer
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    start();
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Page 1: Assessment Interactions</h1>
      <p style={{ marginBottom: "20px" }}>
        Welcome to the e-learning assessment example! You will learn how
        assessment interactions are reported to the LRS as{" "}
        <b>xAPI Statements</b>. It’s important to point out that all assessment
        interactions <span className="code-text">(cmi.interaction)</span> must
        have a parent relationship to the overall assessment Activity and a
        grouping relationship to the lesson and course Activities. The content
        developer is responsible for creating the assessment Activity and
        establishing the parent assessment Activity relationship.
      </p>
      <p className="top-bottom-margins">
        Click the <b>NEXT Button</b> to continue to continue to the{" "}
        <span className="code-text">fill-In</span> assessment question example.
      </p>
    </div>
  );
};

export default Intro;
