import React, { useState, useEffect } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button, Radio, RadioGroup } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'


const Choice = () => {
  const dispatch = useApplicationDispatch();
  const xapi = ELearningSingleton.getInstance();
  const [disableButton, setDisableButton] = useState(false);
  const [value, setValue] = React.useState("1");
  const [id, setId] = React.useState('option_1')

  const onChange = (e) => {
    setId(e.target.id);
    setValue(e.target.value)
  };

  const onSubmit = async (e) => {
    // disable submit button
    setDisableButton(true);
    // set assessment answer
    const correct = value === '2' ? true : false;
    const response = id;

    // create choice response pattern to pass into sendInteraction
    const choiceComponentList = [
      { id: "option_1", description: "Sphere" },
      { id: "option_2", description: "Irregularly shaped ellipsoid" },
      { id: "option_3", description: "Flat" }
    ].map(item => xapi.createInteractionComponentList(item.id, item.description));

    // create interaction object to pass to setInteractionResponse
    let interaction = xapi.createInteraction(
      INTERACTION.CHOICE,
      "https://navy.mil/netc/xapi/activities/cmi.interactions/7dd7b31e-ce13-11eb-b8bc-0242ac130003",
      "Question 2",
      "What is the shape of Earth?",
      ["option_2"],
      choiceComponentList
    );
    // send choice interaction response 
    await xapi.sendInteraction(
      [response],
      interaction,
      interactionParent
    ).catch(error => console.log(error));

    // dispatch choice to global state to manage the Statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });
    dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: correct });

    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
  };

  useEffect(() => {
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
  }, [])

  return (
    <div>
      <h1 className="header">Page 2: Choice</h1>
      <p>
        This is an example of a <span className="code-text">choice (interactionType)</span> question in the assessment.
        The answer to this question is “Irregularly shaped ellipsoid”.
      </p>
      <div
        style={{ display: "flex", flexDirection: "column", marginTop: "10px" }}
      >
        <RadioGroup onChange={onChange} isInline value={value} style={{ margin: '1.5rem' }} spacing={4} defaultValue="1">
          <Radio size="lg" id='option_1' value="1">Sphere</Radio>
          <Radio size="lg" id='option_2' value="2">Irregularly shaped ellipsoid</Radio>
          <Radio size="lg" id='option_3' value="3">Flat</Radio>
        </RadioGroup>
        <Button
          className="top-bottom-margins"
          type="button"
          onClick={onSubmit}
          disabled={disableButton}
        >
          Submit
        </Button>
        <p>
          After answering the question, click the <b>Statement Viewer Button</b> to see the example
          <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
          the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
          <span className="code-text"> contextActivity parent</span> references the assessment Activity.
        </p>
        <p className="top-bottom-margins">
          When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to
          the <span className="code-text"> “Choice (yes/no) with explanation”</span> assessment question example.
        </p>
      </div>
    </div>
  );
};

export default Choice;
