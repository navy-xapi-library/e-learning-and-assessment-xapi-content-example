import React, { useState, useEffect } from "react";

import ELearningSingleton from '../ELearningSingleton'

import { Button } from "@chakra-ui/core";

import '../styles.css'

import {
  useApplicationDispatch,
  useApplicationState,
} from "../../context/app-context";

const Scored = () => {
  const [disableButton, setDisableButton] = useState(false);
  const xapi = ELearningSingleton.getInstance();
  const context = useApplicationState();
  const dispatch = useApplicationDispatch();
  // grab assessmentAnswers from global state, so we can use it for some calculations
  const {
    state: { assessmentAnswers },
  } = context;

  const onSubmit = async () => {
    const totalQuestions = assessmentAnswers.length;
    const correctAnswers = assessmentAnswers.filter((item) => item === true)
      .length;
    let finalScore = (correctAnswers / totalQuestions).toFixed(1);

    finalScore = finalScore === "0.0" || finalScore === "1.0" ? Number(finalScore) : finalScore;

    setDisableButton(true);

    // set score statement to the lrs
    // Most of all of the xapi methods return a promise.
    // setScore is one of them so you can use await to wait for 
    // a response to return or use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the 
    // error could be.
    await xapi.setScore(
      xapi.SCORE.SCALED, finalScore
    ).catch(error => console.log(error));

    // dispatch fill-in to global state to manage the statement viewer
    dispatch({ type: "STATEMENT", payload: xapi.statement });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: false});
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
  };

  useEffect(() => {
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({type: "DISABLE_STATEMENT_VIEWER", payload: true});
  }, []);
  
  return (
    <div className="page-container">
      <h1 className="header">Page 6: Scoring the Lesson </h1>
        <p>
          After all the assessment interactions have been answered and a final
          score for the assessment is determined, then a <b>Scored Statement</b> can be
          sent for the associated lesson Activity. The final score of a lesson
          is communicated by using a <b>Terminated Statement</b> that sets
          <span className="code-text"> result.score.scaled.</span> It is also recommended that the
          <span className="code-text"> result.score.scaled</span> value be set to a decimal number between 0 and 1.
          Click the <b>Score Lesson Button</b> to trigger this action and send the
          Statement.
        </p>
        <Button
          className="top-bottom-margins"
          type="button"
          onClick={onSubmit}
          disabled={disableButton}
        >
          Score Lesson Button
        </Button>     
        <p>
          Click the <b>Statement Viewer</b> Button to see the example <b>Scored Statement</b> that was 
          generated. Notice the result value provided for the scaled score.
        </p>
      <p style={{marginTop:"20px"}}>
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to the 
        next page, “Passing or Failing the Lesson”.
      </p>
    </div>
  );
};

export default Scored;
