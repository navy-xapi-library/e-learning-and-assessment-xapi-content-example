import React, { useState, useEffect } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button, Input } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'


const Other = () => {
    const [answer, setAnswer] = useState("");
    const [disableButton, setDisableButton] = useState(false);
    const dispatch = useApplicationDispatch();
    const xapi = ELearningSingleton.getInstance();
    const onSubmit = async (e) => {
        if (answer === "") return;
        // disable submit button
        setDisableButton(true);

        // ELEARNINGXAPI INTERACTION RESPONDED IMPLEMENTATION 
        let interaction = xapi.createInteraction(
            INTERACTION.OTHER,
            "https://navy.mil/netc/xapi/activities/cmi.interactions/bceb7a86-ce13-11eb-b8bc-0242ac130003",
            "Question 12",
            "What are the latitude and longitude coordinates for Stonehenge in decimal degrees?",
            ["51.1789, 1.8262"]
        );
        await xapi.sendInteraction(
            [answer],
            interaction,
            interactionParent
        ).catch(error => console.log(error));

        // dispatch fill-in to global state to manage the Statement viewer
        dispatch({ type: "STATEMENT", payload: xapi.statement });
        // set assessment answer
        dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: answer === "51.1789, 1.8262" });
        // set page complete
        dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
        dispatch({ type: "SHOW_NAVIGATION", payload: true });
        dispatch({ type: "ENABLE_NEXT", payload: true });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    };
    const onChange = (e) => {
        setAnswer(e.target.value.toLowerCase());
    };

    useEffect(() => {
        dispatch({ type: "ENABLE_NEXT", payload: false });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    }, [])

    return (
        <div className="page-container">
            <h1 className="header">Page 12: Other</h1>
            <p>
                This is an example of a <span className="code-text">other (interactionType)</span> question in the assessment.
                The answer to this question is “51.1789, 1.8262”.
            </p>
            <div className="top-bottom-margins">
                <h3 style={{ marginBottom: '1rem' }}>What are the latitude and longitude coordinates for Stonehenge in decimal degrees?</h3>
                <Input type="text" onChange={onChange} />
                <Button width="100%" style={{ marginTop: "20px" }} type="button" onClick={onSubmit} disabled={disableButton}>
                    Submit
                </Button>
            </div>
            <p>
                After answering the question, click the <b>Statement Viewer Button</b> to see the example
                <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
                the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
                <span className="code-text"> contextActivity parent</span> references the assessment activity.
            </p>
            <p className="top-bottom-margins">
                When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to
                <span className="code-text"> “other - upload”</span> assessment question example.
            </p>
        </div>
    );
};

export default Other;
