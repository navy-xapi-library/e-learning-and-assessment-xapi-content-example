import React, { useState, useEffect, useRef } from "react";

import { useApplicationDispatch } from "../../context/app-context";
import { Button } from "@chakra-ui/core";

import { interactionParent, INTERACTION } from "../configuration";
import ELearningSingleton from '../ELearningSingleton'

import { DragAndDrop, Droppable, Draggable } from "../../drag-n-drop";

const answersMap = {
    target1: 'choice1',
    target2: 'choice2',
    target3: 'choice3',
    target4: 'choice4'
}

const Sequencing = () => {
    const [answer, setAnswer] = useState([]);
    const [disableButton, setDisableButton] = useState(false);
    const [matching, setMatching] = useState([]);
    const dispatch = useApplicationDispatch();
    const xapi = ELearningSingleton.getInstance();

    const checkComplete = () => {
        const correctAnswers = matching.filter(answer => answer.source === answersMap[answer.target])

        if (matching.length === Object.keys(answersMap).length) {
            onSubmit(answer, correctAnswers.length === Object.keys(answersMap).length)
        }
    }

    const onDrop = React.useCallback((e) => { 
        e.source.style.backgroundColor = "#0062cc";
        e.source.style.color = "white";
        e.source.style.border = "none"
        e.source.style.pointerEvents = "none"
        setMatching([...matching, { source: e.source.id, target: e.target.id }])
        setAnswer([...answer, e.source.id])
    })

    const onSubmit = async (answers, success) => {
        // if (answer === "") return;
        // disable submit button
        setDisableButton(true);

        const sequencingComponentList = [
            { id: "choice1", description: "a" },
            { id: "choice2", description: "b" },
            { id: "choice3", description: "c" },
            { id: "choice4", description: "d" },
        ].map(item => xapi.createInteractionComponentList(item.id, item.description));

        // ELEARNINGXAPI INTERACTION RESPONDED IMPLEMENTATION 
        let interaction = xapi.createInteraction(
            INTERACTION.SEQUENCING,
            "https://navy.mil/netc/xapi/activities/cmi.interactions/cb0b89e4-ce13-11eb-b8bc-0242ac130003",
            "Question 9",
            "Order the letters in alphabetic order.",
            xapi.createResponsePattern(INTERACTION.SEQUENCING, ['choice1', 'choice2', 'choice3', 'choice4']),
            sequencingComponentList
        );
        await xapi.sendInteraction(
            xapi.createResponsePattern(INTERACTION.SEQUENCING, answers),
            interaction,
            interactionParent
        ).catch(error => console.log(error));

        // dispatch fill-in to global state to manage the Statement viewer
        dispatch({ type: "STATEMENT", payload: xapi.statement });
        // set assessment answer
        dispatch({ type: "SET_ASSESSMENT_ANSWER", payload: success });
        // set page complete
        dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
        dispatch({ type: "SHOW_NAVIGATION", payload: true });
        dispatch({ type: "ENABLE_NEXT", payload: true });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    };

    useEffect(() => {
        dispatch({ type: "ENABLE_NEXT", payload: false });
        dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    }, [])

    useEffect(() => {
        checkComplete()
    }, [matching])

    return (
        <div className="page-container">
            <h1 className="header">Page 9: Sequencing</h1>
            <p>
                This is an example of a <span className="code-text">sequencing (interactionType)</span> question in the assessment.
                Order the letters in alphabetic order. The answer to this question is “a”, “b”, “c”, and “d”.
            </p>
            <div className="app-container" style={{ height: '8rem' }}>
                <DragAndDrop onDrop={onDrop} >
                    <Draggable className='drag-container'>
                        <div id='choice3' className='drag-element'>c</div>
                        <div id='choice2' className='drag-element'>b</div>
                        <div id='choice1' className='drag-element'>a</div>
                        <div id='choice4' className='drag-element'>d</div>

                    </Draggable>
                    <Droppable className='drop-container'>
                        <div id='slot1' className='drop-element'>slot1</div>
                        <div id='slot2' className='drop-element'>slot2</div>
                        <div id='slot3' className='drop-element'>slot3</div>
                        <div id='slot4' className='drop-element'>slot4</div>
                    </Droppable>
                </DragAndDrop>
            </div>
            <p>
                After answering the question, click the <b>Statement Viewer Button</b> to see the example
                <b> Responded Statement</b> that was generated. Notice the question, <span className="code-text">interactionType</span>, and
                the response. The response is provided in the <span className="code-text">result</span> Object. Also notice the
                <span className="code-text"> contextActivity parent</span> references the assessment activity.
            </p>
            <p className="top-bottom-margins">
                When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to
                <span className="code-text"> likert</span> assessment question example..
            </p>
        </div>
    );
};

export default Sequencing;
