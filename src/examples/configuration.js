// library can be included in a script tag
//<script src="../elearningXAPI.js"></script>
import ELearningSingleton from "./ELearningSingleton";

import base64 from "base-64";
import Chance from "chance";

// holds login info
export let config = {};

export let interactionParent;
export let INTERACTION;


export const eLearningSetUp = async (credentials, cb) => {
  config = {
    endpoint: credentials.endpoint,
    auth:
      "Basic " +
      base64.encode(`${credentials.username}:${credentials.password}`),
    actor: {
      name: credentials.name,
      objectType: "Agent",
      account: {
        homePage: "https://edipi.navy.mil",
        name: new Chance().string({ length: 10, pool: '0123456789' })
      },
    },
    platform: "Moodle 3.8.3"
  };

  // instantiate lib and pass in the config object
  // Since this is a standalone lesson and not part of a LMS a configuration object will be used.
  const xapi = ELearningSingleton.getInstance(config);
  // grab extensions constants
  const { SCHOOL_CENTER, LAUNCH_LOCATION, USER_AGENT } = xapi.EXTENSIONS
  const extensions = {
    [SCHOOL_CENTER]: "Department of Defense (DOD)",
    [LAUNCH_LOCATION]: "Ashore",
    [USER_AGENT]: navigator.userAgent,
  };

  // adding extensions for this attempt
  xapi.addExtensions(extensions);

  // grab interaction constants
  INTERACTION = xapi.INTERACTION;

  // the assessment interactions & results lesson will use
  // an interactionParent with each setInteractionResponse call.
  // Only one parent is needed for all of the interactions in this case
  // since all interactions fall under the assessment section.

  interactionParent = xapi.createParent(
    "https://navy.mil/netc/xapi/activities/assessments/37823a7a-afee-42aa-c4ee-3333acac402",
    "E-Learning Content Assessment",
    "Learn how assessment interactions are reported to the LRS as xAPI Statements."
  );

  await xapi.launch()
    .then(_ => {
      console.log('launch successful');
      cb();
    })
    .catch(error => console.warn(error));
};
