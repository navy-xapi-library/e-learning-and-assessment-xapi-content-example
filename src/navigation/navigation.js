import React from "react";

// context
import {
  useApplicationState,
  useApplicationDispatch,
} from "../context/app-context";

import { Button, ButtonGroup, IconButton } from "@chakra-ui/core";

const Navigation = () => {
  const context = useApplicationState();
  const dispatch = useApplicationDispatch();

  const { state } = context;
  // currentPage, totalPages,
  const { enableNext, showNavigation } = state;

  // console.log("Navigation", data, "pageComplete", pageCompleted);
  const onNext = (e) => {
    e.preventDefault();
    dispatch({ type: "NEXT", payload: null });
  };


  return (
    <ButtonGroup
      spacing={4}
      pos="fixed"
      bottom="5%"
      right="5%"
      id="nav-container"
    >
      {showNavigation && (
        <>
          <Button
            id="large-next-button"
            border="0"
            bg="#003366"
            color="#fff"
            variant="solid"
            onClick={onNext}
            disabled={!enableNext}
            boxShadow="sm"
            _hover={{ boxShadow: "md" }}
            _active={{ boxShadow: "lg" }}
          >
            Next
          </Button>
          <IconButton
            id="small-next-button"
            onClick={onNext}
            disabled={!enableNext}
            boxShadow="sm"
            color="#fff"
            bg="#003366"
            icon="chevron-right"
          ></IconButton>
        </>
      )}
      {/* <Button variantColor="teal" variant="solid" onClick={ goTo } >GoTo</Button> */}
    </ButtonGroup>
  );
};

export default Navigation;
