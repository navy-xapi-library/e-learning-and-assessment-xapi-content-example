/**
 * my simple attempt at a drag and drop component
 * not finished in any means just needed to get this done fast
 */
import * as React from 'react';

import { deepMap, hasChildren } from 'react-children-utilities';

function getOffsetRect(elem) {
  var box = elem.getBoundingClientRect();

  var body = document.body;
  var docElem = document.documentElement;

  var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
  var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

  var clientTop = docElem.clientTop;
  var clientLeft = docElem.clientLeft;


  var top = box.top + scrollTop - clientTop;
  var left = box.left + scrollLeft - clientLeft;
  var bottom = top + (box.bottom - box.top);
  var right = left + (box.right - box.left);

  return {
      top: top,
      left: left,
      bottom: Math.round(bottom),
      right: Math.round(right),
  }
}
const convertRestArgsIntoStylesArr = ([...args]) => {
  return args.slice(1);
}

const getStyles = function () {
  const args = [...arguments];
  const [element] = args;

  let stylesProps = [...args][1] instanceof Array ? args[1] : convertRestArgsIntoStylesArr(args);

  const styles = window.getComputedStyle(element);
  const stylesObj = stylesProps.reduce((acc, v) => {
      acc[v] = styles.getPropertyValue(v);
      return acc;
  }, {});

  return stylesObj;
};

const dragDropReducer = (state, action) => {
  switch (action.type) {
    case 'ON_DRAG_SOURCE':
    case 'ON_DROP_SOURCE':
    case 'ON_DRAG_END':
    case 'ON_DRAG_ENTER':
    case 'ON_DRAG_EXIT':
    case 'ON_DRAG_LEAVE':
    case 'ON_DRAG_OVER':
    case 'ON_DRAG_START':
    case 'ON_DROP':
      break;
    default:
      throw Error(`Not a drag and drop api type ${action.type}`)
  }
}

const dragndropInitialState = {
  dropEffect: 'copy',
  snapBack: false,
  stack: true,
  replace: false
}
const log = console.log


export const DragAndDrop = ({ onDrop, onDrag , onOver, children }) => {
  const [state, dispatch] = React.useReducer(dragDropReducer, dragndropInitialState);

  const onDragHandler = (e) => {
    // console.log('onDrag', e)
    e.dataTransfer.setData('text/plain', e.target.id)
    e.dataTransfer.dropEffect = 'copy'
    if(onDrag) onDrag(e)
  }

  const onOverHandler = (e) => {
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy'  
    if(onOver) onOver(e)
  }

  const onDropHandler = (e) => { 
    const container = document.getElementById('droppableContainer');
    const data = e.dataTransfer.getData('text/plain');
    const source = document.getElementById(data);

    let mlOffset = getStyles(e.target, 'margin-left')['margin-left'].replace('px', '')
    let mtOffset = getStyles(e.target, 'margin-top')['margin-top'].replace('px', '')

    if (mlOffset === '') mlOffset = 0;
    if (mtOffset === '') mtOffset = 0;
    
    source.style.position = 'absolute';
    source.style.left = e.target.offsetLeft - mlOffset +'px'
    source.style.top = e.target.offsetTop - mtOffset +'px'

    container.appendChild(source)
    e.target.style = 'opacity:0'

    if(onDrop) onDrop({source, target:e.target})
  }
  return deepMap(children, child => {
    // console.log(child.type.name)
    if (child.type.name === 'Draggable' && hasChildren(child)) {
      return React.cloneElement(child, {
        ...child.props,
        draggable: true,
        onDragStart: onDragHandler,
        children: child.props.children.map(c => React.cloneElement(c, {
          ...c.props,
          draggable: true,
          // style:{position:'absolute'},
          onDragStart: onDragHandler
        })
        )
      })
    }
    else if (child.type.name === 'Droppable') {
      return React.cloneElement(child, {
        ...child.props,
        draggable: true,
        onDragStart: onDragHandler,
        id:'droppableContainer',
        // style:{position:'relative'},
        children: child.props.children.map(c => React.cloneElement(c, {
          ...c.props,
          onDragOver: onOverHandler,
          onDrop: onDropHandler,
        })
        )
      })
    } else {
      return child
    }
  })
}

export const Draggable = (props) => <div {...props}>{props.children}</div>
export const Droppable = (props) => <div {...props}>{props.children}</div>