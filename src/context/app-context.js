import React, { createContext, useReducer, useContext } from "react";

import { ruuid } from "../xapi-configurations/xapi-configurations";

const initialState = {
  showWelcome: true,
  showTerminateCourse: false,
  courseTerminated: false,
  data: null,
  statement: null,
  showFeedback: false,
  courseCompleted: false,
  currentLesson: 0,
  currentPage: -1,
  totalLessons: null,
  totalPages: null,
  lessonsComplete: null,
  pageCompleted: false,
  showNavigation: true,
  bookmark: null,
  assessmentAnswers: [],
  result: {},
  enableNext: false,
  enablePrevious: false,
  attemptIDs: null,
  registration: "40d95570-bbf5-11ea-b3de-0242ac130004",
  courseRegistration: "3b9d37ac-bbf5-11ea-b3de-0242ac130004",
  courseInitialized: false,
  sideDrawerItems: null,
  disableStatementViewer: true,
  showModal: false,
  hasLogin: false,
  credentials: null,
  hideSideDrawer: true,
  hideStatementViewer: true,
  // nextLesson: null
};

const applicationReducer = (state, action) => {
  // console.log("applicationReducer", state, action);
  const {
    currentLesson,
    currentPage,
    lessonsComplete,
    data,
    totalPages,
    totalLessons,
    // nextLesson,
    courseCompleted,
    showTerminateCourse,
    sideDrawerItems,
  } = state;
  switch (action.type) {
    case "SET_CREDENTIALS":
      return { ...state, credentials: action.payload };
    case "HIDE_LOGIN":
      return { ...state, hasLogin: action.payload };
    case "HIDE_SIDE_DRAWER":
      return { ...state, hideSideDrawer: action.payload };
    case "ENABLE_SIDE_DRAWER_ITEMS":
      return { ...state, sideDrawerItems: action.payload };
    case "SET_COURSE_INITIALIZED":
      return { ...state, courseInitialized: action.payload };
    case "SET_COURSE_DATA":
      return { ...state, data: action.payload };
    case "SET_COURSE_COMPLETE":
      return { ...state, courseCompleted: true };
    case "SET_COURSE_TERMINATED":
      return { ...state, courseTerminated: action.payload };
    case "SET_BOOKMARK":
      return { ...state, bookmark: action.payload };
    case "SET_TOTAL_LESSON":
      return {
        ...state,
        totalLessons: action.payload,
        lessonsComplete: new Array(action.payload).fill(false),
        sideDrawerItems: new Array(action.payload).fill(false),
        attemptIDs: new Array(action.payload).fill(null).map((item) => ruuid()),
        // registrationIDs: new Array(action.payload)
        //   .fill(null)
        //   .map((item) => ruuid()),
      };
    case "SET_TOTAL_PAGES":
      return { ...state, totalPages: action.payload };

    case "SET_LESSON_COMPLETE":
      return {
        ...state,
        bookmark: null,
        lessonsComplete: lessonsComplete.map((item, index) =>
          index === currentLesson ? (item = true) : item
        ),
      };
    case "NEXT":
      // if nextLesson is set then the next button will go to the next lesson instead of the next page
      let nextLesson = null;
      if (lessonsComplete[currentLesson] === true) {
        if (currentLesson === totalLessons - 1) {
          if (lessonsComplete[currentLesson - 1] !== true) {
            nextLesson = currentLesson - 1;
          }
        } else if (
          currentLesson < totalLessons - 1 &&
          lessonsComplete[currentLesson + 1] !== true
        ) {
          nextLesson = currentLesson + 1;
        }
      }
      // console.log("nextLesson", nextLesson, currentLesson, currentPage, totalPages);
      return {
        ...state,
        pageCompleted: false,
        showWelcome: false,
        showFeedback: false,
        currentPage:
          nextLesson !== null
            ? 0
            : currentPage !== totalPages - 1
            ? currentPage + 1
            : currentPage,
        currentLesson: nextLesson !== null ? nextLesson : currentLesson,
        totalPages:
          nextLesson !== null
            ? data.lessons[nextLesson].pages.length
            : totalPages,
        showTerminateCourse: courseCompleted
          ? courseCompleted
          : showTerminateCourse,
        result: nextLesson !== null ? {} : state.result,
      };
    case "ENABLE_NEXT":
      return { ...state, enableNext: action.payload };
    case "ENABLE_PREVIOUS":
      return { ...state, enablePrevious: action.payload };
    case "PREVIOUS":
      return {
        ...state,
        showFeedback: false,
        currentPage: currentPage > 0 ? currentPage - 1 : currentPage,
      };
    case "SET_CURRENT_LESSON":
      let length = data.lessons[action.payload].pages.length;
      let pageNumber = 0;
      const { bookmark } = state;
      if (bookmark) {
        let lessonAndPage = bookmark.split("|");
        pageNumber = Number(lessonAndPage[1].replace("p", ""));
      }
      return {
        ...state,
        currentLesson: action.payload,
        totalPages: length,
        currentPage: pageNumber,
        showWelcome: false,
        result: {},
      };
    // case "SET_NEXT_LESSON":
    //   return { ...state, nextLesson : action.payload}
    case "SET_CURRENT_PAGE":
      return { ...state, currentPage: action.payload };
    // for the sake of time page complete will handle using a global var instead within the page object
    case "SET_PAGE_COMPLETE":
      return { ...state, pageCompleted: true };
    case "SET_ASSESSMENT_ANSWER":
      return {
        ...state,
        assessmentAnswers: [...state.assessmentAnswers, action.payload],
      };
    case "SET_ASSESSMENT_SCORE":
      return { ...state, finalScore: action.payload };
    case "SET_RESULT":
      return {
        ...state,
        result: { ...state.result, ...action.payload },
      };
    case "SHOW_NAVIGATION":
      return { ...state, showNavigation: action.payload };
    case "HIDE_STATEMENT_VIEWER":
      return { ...state, hideStatementViewer: action.payload}
    case "DISABLE_STATEMENT_VIEWER":
      return { ...state, disableStatementViewer: action.payload };
    case "SHOW_WELCOME_PAGE":
      return { ...state, showWelcome: action.payload };
    case "SHOW_COURSE_TERMINATE_PAGE":
      return { ...state, showWelcome: action.payload };
    case "RESET_COURSE":
      return {
        ...state,
        courseInitialized: false,
        showWelcome: true,
        showTerminateCourse: false,
        courseTerminated: false,
        statement: null,
        showFeedback: false,
        courseCompleted: false,
        currentLesson: 0,
        currentPage: -1,
        result: {},
        totalPages: data.lessons[0].pages.length,
        pageCompleted: false,
        showNavigation: true,
        bookmark: null,
        assessmentAnswers: [],
        lessonsComplete: new Array(totalLessons).fill(false),
        sideDrawerItems: new Array(totalLessons).fill(false),
        attemptIDs: new Array(totalLessons).fill(null).map((item) => ruuid()),
        // registrationIDs: new Array(totalLessons)
        //   .fill(null)
        //   .map((item) => ruuid()),
      };
    case "SET_COURSE_REGISTRATION":
        return { ...state, courseRegistration : action.payload }
    case "SET_LESSON_REGISTRATION":
      return { ...state, registration : action.payload }
    case "LOADING":
      return { ...state, loading: true };
    case "SUCCESS":
      return { ...state, loading: false, error: null };
    case "ERROR":
      return { ...state, loading: false, error: action.payload };
    case "STATEMENT":
      return { ...state, statement: action.payload };
    case "FEEDBACK":
      return { ...state, showFeedback: action.payload };
    default:
      return state;
  }
};

const ApplicationStateContext = createContext();
const ApplicationDispatchContext = createContext();

const ApplicationProvider = ({ data, children }) => {
  const [state, dispatch] = useReducer(applicationReducer, initialState);
  return (
    <ApplicationStateContext.Provider value={{ state, data }}>
      <ApplicationDispatchContext.Provider value={dispatch}>
        {children}
      </ApplicationDispatchContext.Provider>
    </ApplicationStateContext.Provider>
  );
};

const useApplicationState = () => {
  const context = useContext(ApplicationStateContext);

  if (context === undefined) {
    throw new Error(
      "useApplicationState must be used within a ApplicationProvider"
    );
  }
  return context;
};

const useApplicationDispatch = () => {
  const context = React.useContext(ApplicationDispatchContext);
  if (context === undefined) {
    throw new Error(
      "useApplicationDispatch must be used within a ApplicationProvider"
    );
  }
  return context;
};

export { ApplicationProvider, useApplicationState, useApplicationDispatch };
