// Interactive Components
// import IconComponent from "../interactive-components/icon-component";
// import SearchInput from '../interactive-components/search-result';
// import CheckboxList from '../interactive-components/checkbox-list';
// import Attachment from '../interactive-components/attachment';
// import LikeDislike from '../interactive-components/liked-disliked';

import Initialized from '../examples/lesson-attempt/initialized';
import Suspended from '../examples/lesson-attempt/suspended';
// import ResumeInstructions from '../examples/lesson-attempt/resumed-instructions';
import Resumed from '../examples/lesson-attempt/resumed';
import TerminatedLessonAttempt from '../examples/lesson-attempt/terminated-lesson-attempt';


import Intro from '../examples/assessment-interactions-results/intro';
import TrueFalse from '../examples/assessment-interactions-results/true-false';
import Choice from '../examples/assessment-interactions-results/choice';
import ChoiceWithExplanation from '../examples/assessment-interactions-results/choice-with-explanation';
import FillIn from '../examples/assessment-interactions-results/fill-in';
import LongFillIn from '../examples/assessment-interactions-results/long-fill-in';
import Matching from '../examples/assessment-interactions-results/matching';
import Performance from '../examples/assessment-interactions-results/performance';
import Numeric from '../examples/assessment-interactions-results/numeric';
import Scored from '../examples/assessment-interactions-results/scored';
import PassedFailed from '../examples/assessment-interactions-results/passed-failed';
import Completed from '../examples/assessment-interactions-results/completed';
import TerminatedAssessmentAttempt from '../examples/assessment-interactions-results/terminate-assessment-attempt';
import Sequencing from '../examples/assessment-interactions-results/sequencing';
import Likert from '../examples/assessment-interactions-results/likert';
import Other from '../examples/assessment-interactions-results/other';
import OtherUpload from '../examples/assessment-interactions-results/other-upload';
import OtherUploadAttachment from '../examples/assessment-interactions-results/other-upload-attachment';
import ChoiceMultiResponse from '../examples/assessment-interactions-results/choice-with-multiple-responses';


export const getPageComponent = (type) =>{
  // console.log(type)
  let pages = {}
  // Attempt Lesson
  pages["initialized"] = Initialized;
  pages["suspended"] = Suspended;
  pages["resumed"] = Resumed;
  pages["terminated"] = TerminatedLessonAttempt;

  // Interactions & Result Lesson
  pages["intro"] = Intro;
  pages["true-false"] = TrueFalse;
  pages["choice"] = Choice;
  pages["choice-with-explanation"] = ChoiceWithExplanation;
  pages['choice-with-multiple-responses'] = ChoiceMultiResponse;
  pages["fill-in"] = FillIn;
  pages["long-fill-in"] = LongFillIn;
  pages["matching"] = Matching;
  pages["performance"] = Performance;
  pages["sequencing"] = Sequencing;
  pages["likert"] = Likert
  pages["other"] = Other;
  pages["other-upload"] = OtherUpload;
  pages["other-upload-attachment"] = OtherUploadAttachment;
  pages["numeric"] = Numeric;
  pages["scored"] = Scored;
  pages["passed-failed"] = PassedFailed;
  pages["completed"] = Completed;
  pages["terminated-assessment"] = TerminatedAssessmentAttempt;

  return pages[type.toLowerCase()]
}